package webput.core;

import java.util.ArrayList;

import fetcher.queries.Instance;
import webput.datastructure.HorCWithCF;
import webput.datastructure.ValuesWithCF;

/**
 *@auther Graberial 
 *@version 2015年1月9日下午5:01:06
 */
public interface Imputation {
	/** update the confidence of HorCWithCF given the corresponding complete Instances
	 * @param horc HorCWithCF to be trained
	 * @param insts   List of Instances that 
	 * @return The confidence of the given HorC
	 */
	public void updateHorCWithInstances(HorCWithCF horc,ArrayList<Instance> insts,ArrayList<String> dictionary);
	
	/** retrieve value for one blank
	 * @param leveragedValues
	 * @param dictionary
	 * @return imputed value with confidence
	 */
	public String retrieveBlankWithHorC(Instance inst, ArrayList<String> dictionary,int type);
	
	public ArrayList<String> genAuxiliaryInformation(ArrayList<Instance> insts);
}

