package webput.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import common.utils.MapUtil;
import database.connection.DbConnection;
import database.processer.TableHandler;
import fetcher.queries.Instance;
import webput.datastructure.CHorCWithCF;
import webput.datastructure.HorCWithCF;
import webput.datastructure.HorCWithCFStore;
import webput.datastructure.ValuesWithCF;

/**
 *@auther Graberial 
 *@version 2015年1月6日上午11:11:06
 */
public class WebPut {
	//public ArrayList<ValuesWithCF>  intermediaTable;
    public float thresholdForQueryConfidence;
    public float thresholdForQueryCoverage;
    public float thresholdForTerms;
    public float percentageOfBlanks;
    public int numOfBlanks;
    public Set<String> keySet;
    public TableHandler tables;
	public HorCWithCFStore myHorCWithCFStore;
	public Imputation IEType;
	//下面的路径硬编码，需要改成配置文件
	public static String tempPath ="C:/Users/Graberial/workspace/webput/WebContent/WEB-INF/classes/";
	
	public static WebPut webput;
	
	public  void init(){
		
		Set<String> keys = new HashSet<String>();
		keys.add("NAME");
		keys.add("EMAIL");
		
		//WebPut webput = new WebPut("personinfo",0.6f,0.7f,0.4f,2,keys);//初始化webput要用到的一些参数
		webput.tables.setTableName("personinfo");
		webput.thresholdForQueryConfidence = 0.6f;
		webput.thresholdForQueryCoverage = 0.7f;
		webput.percentageOfBlanks =0.4f;
		webput.thresholdForTerms =2;
		webput.keySet.addAll(keys);
		
		webput.readImputationTable();//读取数据库中的表格

		webput.tables.checkTupleCompleteness();
		System.out.println(webput.tables.getIncompleteTupleIndexes());
		Cooccur coocur = new Cooccur();//Co-occurance based method 
		webput.setImputationMethod(coocur);
		if(!webput.myHorCWithCFStore.loadFromFile(tempPath+"res/HorCWithCFStore/allHorCs.obj"))
		{
		/*	//下面根据数据库中的不完整表，直接生成查询的各种可能组合HorCWithCF
			int tupleId;
			for(int i=0; i < webput.tables.getIncompleteTupleIndexes().size();i++){
				tupleId = webput.tables.getIncompleteTupleIndexes().get(i);
				ArrayList<Instance> insts = new ArrayList<Instance>();
				insts = webput.tables.getIncompleteInstanceByTupleIndex(tupleId);
			
				for(int t =0; t < insts.size();t++){
					System.out.println(insts.get(t).getLeveragedAttributes()+"->"+insts.get(t).getTargetAttribute());
					ArrayList<HorCWithCF> allHorCs =new ArrayList<HorCWithCF>();
					allHorCs.addAll(webput.getAllHorCWithCF(insts.get(t).getLeveragedAttributes(),insts.get(t).getTargetAttribute(), 2));
					
					for(int k = 0; k < allHorCs.size();k++){
						webput.myHorCWithCFStore.addNewHorCWithCF(allHorCs.get(k));
					}
				//	webput.myHorCWithCFStore.addNewHorCWithCF(pattern);(webput.getAllHorCWithCF(insts.get(t), 2));
				//	myHorCWithCFStore.addAllNewHorCWithCF(webput.getAllHorCWithCF(insts.get(t), 1));
				}
			}*/
			ArrayList<String> tupleAttributes = new ArrayList<String>();
			System.out.println(webput.tables.getAllAttributes());
			tupleAttributes.addAll(webput.tables.getAllAttributes());
			
			for(int i =0; i < tupleAttributes.size();i++){
				ArrayList<String> leveragedAttributes = new ArrayList<String>();
				leveragedAttributes.addAll(tupleAttributes);
				leveragedAttributes.remove(i);
				webput.myHorCWithCFStore.addAllNewHorCWithCF(webput.getAllHorCWithCF(leveragedAttributes,tupleAttributes.get(i),2));
			}
		//	webput.myHorCWithCFStore.saveIntoFile("res/HorCWithCFStore/allHorCs.obj");
			//至此，用完整的实例，测试每个HorCWithCF的可信度
			ArrayList<Instance> insts = new ArrayList<Instance>();
			ArrayList<String> dictionary = new ArrayList<String>();
	
			for(int i =0; i < webput.myHorCWithCFStore.size();i++){ //	for(int i =0; i < webput.myHorCWithCFStore.size();i++){
				HorCWithCF horc = new HorCWithCF();
				horc = webput.myHorCWithCFStore.get(i);
			
				insts = webput.tables.getCompleteInstancesByAttributes(horc.leveragedAttributes,horc.targetAttribute);
				
				dictionary = webput.tables.getDictionaryBykey(horc.targetAttribute);
				
				webput.IEType.updateHorCWithInstances(webput.myHorCWithCFStore.get(i), insts, dictionary);
				
	        
			}
			webput.myHorCWithCFStore.saveIntoFile(tempPath+"res/HorCWithCFStore/allHorCs.obj");
	
		}	
		else{
			for(int i =0; i < webput.myHorCWithCFStore.size();i++){
				System.out.println(webput.myHorCWithCFStore.get(i).leveragedAttributes+"-->"
			+webput.myHorCWithCFStore.get(i).targetAttribute+"   : "+webput.myHorCWithCFStore.get(i).confidence);
			}
		}
		
		//webput.baseline(2);
		//webput.greedyAlgorithm(2);
	}
	
	/**
	 * 默认构造函数
	 */
	public WebPut(){
	//	intermediaTable = new ArrayList<ValuesWithCF>();
		myHorCWithCFStore = new HorCWithCFStore();
		thresholdForQueryConfidence = -1f;
		thresholdForQueryCoverage = -1f;
		keySet = new HashSet<String>();
		tables = new TableHandler();
	}
	/**构造函数，初始化WebPut系统的一些参数
	 * @param tableName  用于测试的表的表名
	 * @param queryCFThreshold  查询的可信度阈值
	 * @param queryCoverThreshold 查询的覆盖率阈值
	 * @param termsToUse  对于coor方法，使用的expansion term的阈值
	 * @param percentageOfBlanks  选择使用空缺率是多少的不完整表
	 * @param keySet   表的主属性
	 */
	public WebPut(String tableName,float queryCFThreshold,float queryCoverThreshold,float percentageOfBlanks,float termsToUse,Set<String> keySet){
		//intermediaTable = new ArrayList<ValuesWithCF>();
		myHorCWithCFStore = new HorCWithCFStore();
		this.keySet = new HashSet<String>();
		tables = new TableHandler();
		tables.setTableName(tableName);
		this.keySet.addAll(keySet);
		this.thresholdForQueryConfidence = queryCFThreshold;
		this.thresholdForQueryCoverage = queryCoverThreshold;
		this.thresholdForTerms = termsToUse;
		this.percentageOfBlanks = percentageOfBlanks;
	}
	public void readImputationTable(){
		//首先初始化table表的sampledTable 和sampledTableWithBlanks
		this.tables.readSampledTable();
		this.numOfBlanks = this.tables.readSampledTableWithBlanks(keySet,percentageOfBlanks);	
	}
	public void setImputationMethod(Imputation IEType){
	    this.IEType = IEType;
	}
	
	public static synchronized WebPut getWebPut(){
		if (null == webput) {
			webput = new WebPut();
			webput.init();
		}
		return webput;
	}
	public void baseline(int type){
		int tupleId =0;
		for(int i=0; i < tables.getIncompleteTupleIndexes().size();i++){
			tupleId = tables.getIncompleteTupleIndexes().get(i);
			ArrayList<Instance> insts = new ArrayList<Instance>();
			System.out.println("before imputation:"+tables.sampledTableWithBlanks.get(tupleId));
			System.out.println("before imputation:"+tables.valueConfidences.get(tupleId));
			insts = tables.getIncompleteInstanceByTupleIndex(tupleId);//元组的所有可用属性-->未知属性的Instances
		
		
			for(int t =0; t < insts.size();t++){//allLeveragedValues -->targetValue
				Instance fullInstance = new Instance();
				fullInstance = insts.get(t);
				ArrayList<HorCWithCF> allHorCs = new ArrayList<HorCWithCF>();
				allHorCs.addAll(getAllHorCWithCF(insts.get(t).getLeveragedAttributes(),insts.get(t).getTargetAttribute(), type));
			
				float maxQueryCF = 0f;
				int maxCFIndex = 0;
				for(int j = 0; j < allHorCs.size();j++)//找出cf值最大的HorCWithCF
				{
					    float confidence =myHorCWithCFStore.getConfidenceForHorC(allHorCs.get(j).leveragedAttributes, allHorCs.get(j).targetAttribute);
					    if(confidence > maxQueryCF){
					    	 maxQueryCF = confidence;
					    	 maxCFIndex = j;
					    }
				}
				//到这里，allHorCs.get(maxCFIndex)模式是最好的查询模式，eg. Name+ Title --> Email，接下来就要生成这样的Instance来查询
				Instance inst = new Instance();
				HorCWithCF best = new HorCWithCF();
				best = allHorCs.get(maxCFIndex);
				for(int k = 0; k < best.leveragedAttributes.size();k++){
					inst.leveragedValues.put(best.leveragedAttributes.get(k), fullInstance.leveragedValues.get(best.leveragedAttributes.get(k)));
					inst.targetValue.put(best.targetAttribute, "");
					inst.tupleId = i;
				}
				//inst是最好的查询实例
			
				String retrievedValue = IEType.retrieveBlankWithHorC(inst, tables.getDictionaryBykey(inst.getTargetAttribute()), type);
				System.out.println("retrieved value for"+inst.getLeveragedValues()+"-->"+inst.getTargetAttribute()+"is ::"+retrievedValue);
				tables.sampledTableWithBlanks.get(tupleId).put(inst.getTargetAttribute(), retrievedValue);
				updateConfidences(tupleId,inst.getLeveragedAttributes(),best.targetAttribute,best.confidence);//计算由inst查询
				
			}
			System.out.println("after imputaion"+tables.sampledTableWithBlanks.get(tupleId));
			System.out.println("after imputaion"+tables.valueConfidences.get(tupleId));
		}
		System.out.println(tables.sampledTableWithBlanks);
		System.out.println("baseline");
	}
	public void greedyAlgorithm(int type){
		//System.out.println("in greedy");
		int imputNums = 0;
		int correctImputedNums = 0;
		for(int i =0; i < tables.getIncompleteTupleIndexes().size();i++){
		
			int tupleId =  tables.getIncompleteTupleIndexes().get(i);
			System.out.println("processing tuple:"+tupleId);
			ArrayList<Instance> insts = new ArrayList<Instance>();
			insts = tables.getIncompleteInstanceByTupleIndex(tupleId);//获取所有的instance :utilizedAttributes -->targetAttribute 
			
			
				System.out.println("before imputation:"+tables.sampledTableWithBlanks.get(tupleId));
				System.out.println("before imputation:"+tables.valueConfidences.get(tupleId));
				//1st, find out the best query that could possibly retrieve the best value for all  the missing value
				HorCWithCF horcForOneBlank,bestHorCForTuple;
				horcForOneBlank = new HorCWithCF();       //best query for one blank
				bestHorCForTuple = new HorCWithCF();      //best query for one tuple
			
				//deal with each instance, get all possible attributes combination to the target attribute, figure out the best one
				// Conf(value) = Conf(q)*Conf(utilized values)
				//ArrayList<Map<HorCWithCF,Float>> imputSequences = new ArrayList<Map<HorCWithCF,Float>>();
				ArrayList<ArrayList<HorCWithCF>> imputSequences = new ArrayList<ArrayList<HorCWithCF>>();
				for(int j = 0; j < insts.size();j++){                    
					Instance inst = new Instance();
					inst = insts.get(j);
					Map<HorCWithCF,Float> sequenceForBlank = new LinkedHashMap<HorCWithCF,Float>();
					// (1)deal with each instance, get all attributes combiantions
					ArrayList<HorCWithCF> allHorCs = new ArrayList<HorCWithCF>();
					allHorCs.addAll(getAllHorCWithCF(inst.getLeveragedAttributes(),inst.getTargetAttribute(),type));
					float conf = 1f;
					
					//calculate the confidence of each missing value
					for(int t = 0; t < allHorCs.size();t++){
						HorCWithCF horc = new HorCWithCF();
						horc = allHorCs.get(t);
						// deal with a particuly query mode
						conf *=this.myHorCWithCFStore.getConfidenceForHorC(horc.leveragedAttributes, horc.targetAttribute);                          //conf(value） = conf(mode)
						
						//计算horc这种查询模式的可获得的值得可信度
						for(String key:horc.leveragedAttributes){
							conf *=Float.parseFloat(tables.valueConfidences.get(tupleId).get(key));
						}
						horc.confidence = conf;
						sequenceForBlank.put(horc, conf);
					}
					sequenceForBlank = MapUtil.sortByValue(sequenceForBlank, MapUtil.SortType.DESC);
				//	System.out.println(sequenceForBlank);
					ArrayList<HorCWithCF> seq = new ArrayList<HorCWithCF>();
					for(HorCWithCF horc:sequenceForBlank.keySet())
					{
						seq.add(horc);
					}
					imputSequences.add(seq);
				} //end of the for loop to find all horcs for the tuple
			
				
				while(insts.size() > 0 && imputSequences.size() > 0){
				
					int index = getAndRemoveBestHorC(imputSequences,bestHorCForTuple);
				     System.out.println(bestHorCForTuple.leveragedAttributes+"--------->"+bestHorCForTuple.targetAttribute);
				//	System.out.println(bestHorCForTuple.leveragedAttributes+"--------->"+bestHorCForTuple.targetAttribute);
					//至此，bestHorCForTuple应该是该元祖上最好的查询模式了，下面用该查询模式来查询targetAttribute的值
					ArrayList<String> dictionary = new ArrayList<String>();
					
					if(bestHorCForTuple.targetAttribute.equals("COUNTRY") || bestHorCForTuple.targetAttribute.equals("TITLE") 
					 || bestHorCForTuple.targetAttribute.equals("ORGANIZATION")){
						dictionary = tables.getDictionaryBykey(bestHorCForTuple.targetAttribute);
					}
					Instance bestInstance = new Instance();
					bestInstance = tables.getInstance(tupleId,bestHorCForTuple.leveragedAttributes,bestHorCForTuple.targetAttribute);
					String retrievedValue = this.IEType.retrieveBlankWithHorC(bestInstance, dictionary, 2);
					System.out.println("retrieved value for"+bestInstance.getLeveragedValues()+"-->"+bestInstance.getTargetAttribute()
							+"is ::"+retrievedValue);
					
					if(retrievedValue.isEmpty())
					{
					
						while(imputSequences.size() > 0 && retrievedValue.isEmpty()){
							bestHorCForTuple.leveragedAttributes.clear();
							bestHorCForTuple.coverIndexes.clear();
							bestHorCForTuple.targetAttribute="";
							bestHorCForTuple.confidence = -1f;
							int bestIndex = getAndRemoveBestHorC(imputSequences,bestHorCForTuple);
							
						     System.out.println(bestHorCForTuple.leveragedAttributes+"--------->"+bestHorCForTuple.targetAttribute);
						     dictionary.clear();
								
							if(bestHorCForTuple.targetAttribute.equals("COUNTRY") || bestHorCForTuple.targetAttribute.equals("TITLE") 
							 || bestHorCForTuple.targetAttribute.equals("ORGANIZATION")){
								dictionary.clear();
								dictionary = tables.getDictionaryBykey(bestHorCForTuple.targetAttribute);
						
							}
							bestInstance = tables.getInstance(tupleId,bestHorCForTuple.leveragedAttributes,bestHorCForTuple.targetAttribute);
							retrievedValue = this.IEType.retrieveBlankWithHorC(bestInstance, dictionary, 2);
							System.out.println("retrieved value for"+bestInstance.getLeveragedValues()+"-->"+bestInstance.getTargetAttribute()
									+"is ::"+retrievedValue);				
						}
						if(imputSequences.size() <= 0){
							break;                    // all HorCWithCF have been used but no value retrieved
						}
				
					}
					imputNums++;
					if(isCorrectRetrieved(retrievedValue,tupleId,bestHorCForTuple.targetAttribute)){
						correctImputedNums++;
					}
					tables.sampledTableWithBlanks.get(tupleId).put(bestHorCForTuple.targetAttribute, retrievedValue);
					updateConfidences(tupleId,bestHorCForTuple.leveragedAttributes,bestHorCForTuple.targetAttribute,bestHorCForTuple.confidence);//计算由inst查询
					System.out.println("after imputaion"+tables.sampledTableWithBlanks.get(tupleId));
					System.out.println("after imputaion"+tables.valueConfidences.get(tupleId));
					insts.clear();
					
					insts =  tables.getIncompleteInstanceByTupleIndex(tupleId);
					imputSequences.clear();
					System.out.println(insts.size());
					for(int j = 0; j < insts.size();j++){                    
						Instance inst = new Instance();
						inst = insts.get(j);
						Map<HorCWithCF,Float> sequenceForBlank = new LinkedHashMap<HorCWithCF,Float>();
						// (1)deal with each instance, get all attributes combiantions
						ArrayList<HorCWithCF> allHorCs = new ArrayList<HorCWithCF>();
						allHorCs.addAll(getAllHorCWithCF(inst.getLeveragedAttributes(),inst.getTargetAttribute(),2));
						float conf = 1f;
						
						//calculate the confidence of each missing value
						for(int t = 0; t < allHorCs.size();t++){
							HorCWithCF horc = new HorCWithCF();
							horc = allHorCs.get(t);
							// deal with a particuly query mode
							conf *=this.myHorCWithCFStore.getConfidenceForHorC(horc.leveragedAttributes, horc.targetAttribute);                          //conf(value） = conf(mode)
							
							//计算horc这种查询模式的可获得的值得可信度
							for(String key:horc.leveragedAttributes){
								conf *=Float.parseFloat(tables.valueConfidences.get(tupleId).get(key));
							}
							System.out.println("confidence of "+horc.leveragedAttributes+" to "+horc.targetAttribute+"is"+conf);
							horc.confidence = conf;
							sequenceForBlank.put(horc, conf);
						}
						sequenceForBlank = MapUtil.sortByValue(sequenceForBlank, MapUtil.SortType.DESC);
					//	System.out.println(sequenceForBlank);
						ArrayList<HorCWithCF> seq = new ArrayList<HorCWithCF>();
						for(HorCWithCF horc:sequenceForBlank.keySet())
						{
							seq.add(horc);
						}
						imputSequences.add(seq);
						
					} //end of the for loop to find all horcs for the tuple
					
			}
		}
		System.out.println("Precision :"+(float)correctImputedNums / imputNums );
	
		System.out.println("Recall:      "+(float) correctImputedNums / numOfBlanks);
		System.out.println("greedy algorithm");
	}
	
	public void updateConfidences(int tupleId,ArrayList<String> utilizedAttributes,String targetAttribute,float queryConfidence){
		Map<String,String> confidences = new LinkedHashMap<String,String>();
		confidences.putAll(tables.valueConfidences.get(tupleId));
		
		Float valueConfidence = queryConfidence;
		for(String key:utilizedAttributes){
			valueConfidence *= Float.parseFloat(confidences.get(key));
		}
		
		tables.valueConfidences.get(tupleId).put(targetAttribute, valueConfidence.toString());
	}
	/**由一个Instance分析出可以产生的所有HorCWithCF，这样所有的HorCWithCF都可以被初始化分析出来
	 * @param inst
	 * @return
	 */
	public ArrayList<HorCWithCF> getAllHorCWithCF(ArrayList<String> utilizedAttributes, String targetAttribute,int type){
		//System.out.println(inst.getLeveragedAttributes()+"-->"+inst.getTargetAttribute());
		ArrayList<HorCWithCF> allHorC = new ArrayList<HorCWithCF>();
		//HorCWithCFStore  allHorC = new HorCWithCFStore();
		
		ArrayList<ArrayList<String>> allCombinations = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> lastCombinations = new ArrayList<ArrayList<String>>();
	
		//A->B这种类型的查询属性，A只包含一个主属性
		for(int i =0; i< utilizedAttributes.size();i++)
		{	
		// 构造以单个属性作为查询关键词的这个查询模式
			ArrayList<String> attributesCombination = new ArrayList<String>();
			
				attributesCombination.add(utilizedAttributes.get(i));
				lastCombinations.add(attributesCombination);
		//	System.out.println("1111111"+attributesCombination.toString());
		}
		allCombinations.addAll(lastCombinations);
			
			//AB->类型的查询属性，AB当中一个为主属性
		for(int n =2;n<= utilizedAttributes.size();n++)
		{
			ArrayList<ArrayList<String>> tempCombinations = new 	ArrayList<ArrayList<String>>();
			//根据上一次处理结果的Attributes组合，生成本次组合值
			for(int i =0; i < lastCombinations.size();i++ )
			{
				for(int j = i+1;j < lastCombinations.size();j++)
				{
					ArrayList<String> tempCombination = combineAttributes(lastCombinations.get(i),lastCombinations.get(j));
					if(tempCombination.size() == n && !tempCombinations.contains(tempCombination))
					{
						tempCombinations.add(tempCombination);
					}
				}
			}
			lastCombinations.clear();									//lastCombinations记录上一次的Attributes组合
			lastCombinations.addAll(tempCombinations);
			allCombinations.addAll(tempCombinations);
		}
		//接下来判断生成的所有组合值中是否是否都有key，没有key的删除掉。符合条件的生成一个HorCWithCF
	//	System.out.println("keySet"+keySet.toString());
		for(int i = 0; i < allCombinations.size();i++)
		{
			ArrayList<String> tempAttributes = allCombinations.get(i);
			boolean containKey = false;
			for(String key:keySet)
			{
				if(tempAttributes.contains(key))
				{
					containKey = true;
					break;
				}
			}
			if(containKey == false)
			{
				allCombinations.remove(i);
				i--;
			}
			else
			{
				if(type == 2){
					CHorCWithCF horc = new CHorCWithCF();
					Collections.sort(tempAttributes);
					horc.leveragedAttributes = tempAttributes;
				//	System.out.println(tempAttributes);
					horc.targetAttribute = targetAttribute;
					//System.out.println(inst.getTargetAttribute());
					horc.type = type;
				//	System.out.println("utilized:"+tempAttributes+"target"+inst.getTargetAttribute()+"type:"+type);
				//	CHorCWithCF oneHorC = new CHorCWithCF(horc);
					allHorC.add(horc);
				}
				else{
					//这里添加PHorCWithCF的情况
				}
			
			}
		}
		return allHorC;
	}
	/**将两个utilizedAttributes 属性组合成一个，去掉重复的属性
	 * @param one
	 * @param another
	 * @return 
	 */
	public ArrayList<String> combineAttributes(ArrayList<String> one,ArrayList<String> another)
	{
		ArrayList<String> mergedResult = new ArrayList<String>();
		mergedResult.addAll(one);
		for(int i = 0; i < another.size();i++)
		{
			if(!mergedResult.contains(another.get(i)))
			{
				mergedResult.add(another.get(i));
			}
		}
		Collections.sort(mergedResult);
		return mergedResult;
	}
	public int  getAndRemoveBestHorC(ArrayList<ArrayList<HorCWithCF>> imputSequences,HorCWithCF best){
		int bestIndex=-1;
		best.confidence = -1f;
		HorCWithCF it = new HorCWithCF();
		if(imputSequences.size() > 0){
			for(int i = 0; i <imputSequences.size();i++){
				it = imputSequences.get(i).get(0);
			
				if(best.confidence < it.confidence){
					best.leveragedAttributes.clear();
					best.leveragedAttributes.addAll(it.leveragedAttributes);
					best.targetAttribute = it.targetAttribute;
					best.confidence = it.confidence;
					best.coverage = it.coverage;
					bestIndex = i;
				}
			}
			imputSequences.get(bestIndex).remove(0);
			if(imputSequences.get(bestIndex).isEmpty()){
				imputSequences.remove(bestIndex);
			}
		}
		return bestIndex;
	}
	
	public boolean isCorrectRetrieved(String retrievedValue,int tuple,String attribute){
		boolean correct = false;
		if(this.tables.sampledTestTable.get(tuple).get(attribute).equalsIgnoreCase(retrievedValue)){
			return true;
		}
		else{
			return false;
		}
	}
	/**根据网页产生的元组id来处理相应的元组，返回处理后的结果
	 * @param tupleId
	 * @return
	 */
	public Map<String,String> processOneTuple(int tupleId){
		Map<String,String> tuple = new HashMap<String,String>();
		
		int imputNums = 0;
		int correctImputedNums = 0;
		if(!tables.getIncompleteTupleIndexes().contains(tupleId)){
			 tuple = tables.sampledTableWithBlanks.get(tupleId);
			 return tuple;
		}
		tuple = tables.sampledTableWithBlanks.get(tupleId);
		System.out.println("processing tuple:"+tupleId);
		ArrayList<Instance> insts = new ArrayList<Instance>();
		insts = tables.getIncompleteInstanceByTupleIndex(tupleId);//获取所有的instance :utilizedAttributes -->targetAttribute 
		
			System.out.println("before imputation:"+tables.sampledTableWithBlanks.get(tupleId));
			System.out.println("before imputation:"+tables.valueConfidences.get(tupleId));
			//1st, find out the best query that could possibly retrieve the best value for all  the missing value
			HorCWithCF horcForOneBlank,bestHorCForTuple;
			horcForOneBlank = new HorCWithCF();       //best query for one blank
			bestHorCForTuple = new HorCWithCF();      //best query for one tuple
		
			//deal with each instance, get all possible attributes combination to the target attribute, figure out the best one
			// Conf(value) = Conf(q)*Conf(utilized values)
			//ArrayList<Map<HorCWithCF,Float>> imputSequences = new ArrayList<Map<HorCWithCF,Float>>();
			ArrayList<ArrayList<HorCWithCF>> imputSequences = new ArrayList<ArrayList<HorCWithCF>>();
			for(int j = 0; j < insts.size();j++){                    
				Instance inst = new Instance();
				inst = insts.get(j);
				Map<HorCWithCF,Float> sequenceForBlank = new LinkedHashMap<HorCWithCF,Float>();
				// (1)deal with each instance, get all attributes combiantions
				ArrayList<HorCWithCF> allHorCs = new ArrayList<HorCWithCF>();
				allHorCs.addAll(getAllHorCWithCF(inst.getLeveragedAttributes(),inst.getTargetAttribute(),2));
				float conf = 1f;
				
				//calculate the confidence of each missing value
				for(int t = 0; t < allHorCs.size();t++){
					HorCWithCF horc = new HorCWithCF();
					horc = allHorCs.get(t);
					// deal with a particuly query mode
					conf *=this.myHorCWithCFStore.getConfidenceForHorC(horc.leveragedAttributes, horc.targetAttribute);                          //conf(value） = conf(mode)
					
					//计算horc这种查询模式的可获得的值得可信度
					for(String key:horc.leveragedAttributes){
						conf *=Float.parseFloat(tables.valueConfidences.get(tupleId).get(key));
					}
					horc.confidence = conf;
					sequenceForBlank.put(horc, conf);
				}
				sequenceForBlank = MapUtil.sortByValue(sequenceForBlank, MapUtil.SortType.DESC);
			//	System.out.println(sequenceForBlank);
				ArrayList<HorCWithCF> seq = new ArrayList<HorCWithCF>();
				for(HorCWithCF horc:sequenceForBlank.keySet())
				{
					seq.add(horc);
				}
				imputSequences.add(seq);
			} //end of the for loop to find all horcs for the tuple
		
			
			while(insts.size() > 0 && imputSequences.size() > 0){
			
				int index = getAndRemoveBestHorC(imputSequences,bestHorCForTuple);
			     System.out.println(bestHorCForTuple.leveragedAttributes+"--------->"+bestHorCForTuple.targetAttribute);
			//	System.out.println(bestHorCForTuple.leveragedAttributes+"--------->"+bestHorCForTuple.targetAttribute);
				//至此，bestHorCForTuple应该是该元祖上最好的查询模式了，下面用该查询模式来查询targetAttribute的值
				ArrayList<String> dictionary = new ArrayList<String>();
				
				if(bestHorCForTuple.targetAttribute.equals("COUNTRY") || bestHorCForTuple.targetAttribute.equals("TITLE") 
				 || bestHorCForTuple.targetAttribute.equals("ORGANIZATION")){
					dictionary = tables.getDictionaryBykey(bestHorCForTuple.targetAttribute);
				}
				Instance bestInstance = new Instance();
				bestInstance = tables.getInstance(tupleId,bestHorCForTuple.leveragedAttributes,bestHorCForTuple.targetAttribute);
				String retrievedValue = this.IEType.retrieveBlankWithHorC(bestInstance, dictionary, 2);
				System.out.println("retrieved value for"+bestInstance.getLeveragedValues()+"-->"+bestInstance.getTargetAttribute()
						+"is ::"+retrievedValue);
				
				if(retrievedValue.isEmpty())
				{
				
					while(imputSequences.size() > 0 && retrievedValue.isEmpty()){
						bestHorCForTuple.leveragedAttributes.clear();
						bestHorCForTuple.coverIndexes.clear();
						bestHorCForTuple.targetAttribute="";
						bestHorCForTuple.confidence = -1f;
						int bestIndex = getAndRemoveBestHorC(imputSequences,bestHorCForTuple);
						
					     System.out.println(bestHorCForTuple.leveragedAttributes+"--------->"+bestHorCForTuple.targetAttribute);
					     dictionary.clear();
							
						if(bestHorCForTuple.targetAttribute.equals("COUNTRY") || bestHorCForTuple.targetAttribute.equals("TITLE") 
						 || bestHorCForTuple.targetAttribute.equals("ORGANIZATION")){
							dictionary.clear();
							dictionary = tables.getDictionaryBykey(bestHorCForTuple.targetAttribute);
					
						}
						bestInstance = tables.getInstance(tupleId,bestHorCForTuple.leveragedAttributes,bestHorCForTuple.targetAttribute);
						retrievedValue = this.IEType.retrieveBlankWithHorC(bestInstance, dictionary, 2);
						System.out.println("retrieved value for"+bestInstance.getLeveragedValues()+"-->"+bestInstance.getTargetAttribute()
								+"is ::"+retrievedValue);				
					}
					if(imputSequences.size() <= 0){
						break;                    // all HorCWithCF have been used but no value retrieved
					}
			
				}
				imputNums++;
				if(isCorrectRetrieved(retrievedValue,tupleId,bestHorCForTuple.targetAttribute)){
					correctImputedNums++;
				}
				tuple.put(bestHorCForTuple.targetAttribute, retrievedValue);
				tables.sampledTableWithBlanks.get(tupleId).put(bestHorCForTuple.targetAttribute, retrievedValue);
				updateConfidences(tupleId,bestHorCForTuple.leveragedAttributes,bestHorCForTuple.targetAttribute,bestHorCForTuple.confidence);//计算由inst查询
				System.out.println("after imputaion"+tables.sampledTableWithBlanks.get(tupleId));
				System.out.println("after imputaion"+tables.valueConfidences.get(tupleId));
				insts.clear();
				
				insts =  tables.getIncompleteInstanceByTupleIndex(tupleId);
				imputSequences.clear();
				System.out.println(insts.size());
				for(int j = 0; j < insts.size();j++){                    
					Instance inst = new Instance();
					inst = insts.get(j);
					Map<HorCWithCF,Float> sequenceForBlank = new LinkedHashMap<HorCWithCF,Float>();
					// (1)deal with each instance, get all attributes combiantions
					ArrayList<HorCWithCF> allHorCs = new ArrayList<HorCWithCF>();
					allHorCs.addAll(getAllHorCWithCF(inst.getLeveragedAttributes(),inst.getTargetAttribute(),2));
					float conf = 1f;
					
					//calculate the confidence of each missing value
					for(int t = 0; t < allHorCs.size();t++){
						HorCWithCF horc = new HorCWithCF();
						horc = allHorCs.get(t);
						// deal with a particuly query mode
						conf *=this.myHorCWithCFStore.getConfidenceForHorC(horc.leveragedAttributes, horc.targetAttribute);                          //conf(value） = conf(mode)
						
						//计算horc这种查询模式的可获得的值得可信度
						for(String key:horc.leveragedAttributes){
							conf *=Float.parseFloat(tables.valueConfidences.get(tupleId).get(key));
						}
						System.out.println("confidence of "+horc.leveragedAttributes+" to "+horc.targetAttribute+"is"+conf);
						horc.confidence = conf;
						sequenceForBlank.put(horc, conf);
					}
					sequenceForBlank = MapUtil.sortByValue(sequenceForBlank, MapUtil.SortType.DESC);
				//	System.out.println(sequenceForBlank);
					ArrayList<HorCWithCF> seq = new ArrayList<HorCWithCF>();
					for(HorCWithCF horc:sequenceForBlank.keySet())
					{
						seq.add(horc);
					}
					imputSequences.add(seq);
					
				} //end of the for loop to find all horcs for the tuple
			}
			return tuple;	
	}
}
