package webput.core;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import webput.datastructure.*;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import fetcher.implementation.*;

import com.leeonJava.fileDispose.ReadWriteTxt;

import database.objects.Key;
import fetcher.queries.Instance;
import database.processer.TableHandler;
import webput.datastructure.*;
import database.processer.*;
import fetcher.queries.*;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import extractor.base.*;
import extractor.auxiliary.*;
/**
 * @author Graberial
 *
 */
/**
 * @author Graberial
 *
 */
public class Cooccur implements Imputation {
	public	String tableName;
	public ArrayList<Integer> completeTupleIndexes;
	public ArrayList<Integer> incompleteTupleIndexes;
	public Set<String> keySet;

    public int termsToUse;
    public float percentageOfBlanks;
    public String tempPath ="C:/Users/Graberial/workspace/webput/WebContent/WEB-INF/classes/";
    String serializedClassifier  = tempPath +"res/classifiers/english.all.3class.distsim.crf.ser.gz";
	AbstractSequenceClassifier<CoreLabel> classifier = 
		CRFClassifier.getClassifierNoExceptions(serializedClassifier);
    protected static Logger logger = LogManager.getLogger();
	public Cooccur(){
		tableName ="";
		keySet = new HashSet<String>();
	    
		completeTupleIndexes = new ArrayList<Integer>();
		incompleteTupleIndexes = new ArrayList<Integer>();
		termsToUse = 1;
		percentageOfBlanks = -1f;
	
	}
	
	/*public static void main(String[] args){
	CooccuranceMethod co = new CooccuranceMethod("personinfo",2,5,0.2f);
		
		//从数据库中读取不完整的元组，并生成所有HorCWithCF
		HorCWithCFStore horcs = new HorCWithCFStore();
		
		for(int i =0; i < co.incompleteTupleIndexes.size();i++){
		ArrayList<Instance> insts= new ArrayList<Instance>();
		insts = co.tables.getIncompleteInstanceByTupleIndex(co.incompleteTupleIndexes.get(i));
		for(int j=0;j <insts.size();j++){
			horcs.addAllNewHorCWithCF(co.getAllHorCWithCF(insts.get(j),2));
		}
	}

	
	
	/**根据给出的Instance，查询该inst的targetValue
	 * @param inst
	 * @param dictionary
	 * @param type
	 * @return  空缺值
	 */
	public String retrieveBlankWithHorC(Instance inst, ArrayList<String> dictionary,int type){
		String value="";
		ArrayList<String> expansionTerms = new ArrayList<String>();
		ArrayList<String> snippets = new ArrayList<String>();
		
		String searchResponse = Fetcher.graspPosQueryWeb(inst.getLeveragedValues());//没有使用expansionTerms的情况
		snippets = Extractor.extractAbstracts(inst.getLeveragedValues(), searchResponse);
		
		//下面使用expansionTerms重新来查询
		expansionTerms = this.readExpansionTerms(inst.getLeveragedAttributes(), inst.getTargetAttribute(), 5);
		System.out.println("Read expansion terms from file"+expansionTerms);
		if (!expansionTerms.isEmpty())
			{
				ArrayList<String> utilizedValues = new ArrayList<String>();
				//如果没有现有的expansionTerms，则只能生成通过解析返回的网页生成expansionTerms
				//将多余的expansion terms去掉
				while(expansionTerms.size() > termsToUse){
					
					expansionTerms.remove(expansionTerms.size()-1);
				}
			
					for(int i =0; i < termsToUse;i++){
						utilizedValues.addAll(inst.getLeveragedValues());
						utilizedValues.add(expansionTerms.get(i));
						String resp = Fetcher.graspPosQueryWeb(utilizedValues);
						snippets.addAll( Extractor.extractAbstracts(inst.getLeveragedValues(), resp));
					}
				
			}
		value = Extractor.extractValueFromSnippets(classifier,snippets,inst.getTargetAttribute(), dictionary);
		return value;
	} 
	
	/** 根据完整实例产生辅助信息
	 * @see webput.core.Imputation#genAuxiliaryInformation(java.util.ArrayList)
	 */
	public ArrayList<String> genAuxiliaryInformation(ArrayList<Instance> completeInsts){
		SnippetsStore mySnippetStore_train = new SnippetsStore();
		ArrayList<String> expansionTerms = new ArrayList<String>();
		String htmlpage ="";
		for(int i=0;i < completeInsts.size();i++){
			ArrayList<String> leveragedValues = new ArrayList<String>();
			leveragedValues.addAll(completeInsts.get(i).getLeveragedValues());
			leveragedValues.add(completeInsts.get(i).getTargetValue());
			htmlpage = Fetcher.graspPosQueryWeb(leveragedValues);
			
			ArrayList<Snippets> tempSnippets = new ArrayList<Snippets>();
			tempSnippets = Extractor.extractSnippets(leveragedValues,htmlpage);
			mySnippetStore_train.addAll(tempSnippets);
		}
		
		expansionTerms =ExpansionTerms.genExpansionTermsWithGroups(
				mySnippetStore_train, completeInsts, 0.4f);
		if(expansionTerms.isEmpty()){
			System.out.println(" error:no expansion terms generated");
		}
		else
		{
			this.saveExpansionTerms(completeInsts.get(0).getLeveragedAttributes(), completeInsts.get(0).getTargetAttribute(), expansionTerms);	
		}
		return expansionTerms;
	}


	/**离线学习的过程，用完整的Instance，测试HorCWithCF的Cover和Support，计算出这个模式的confidence
	 * @param insts 完整的实例	
	 * @param dictionary  targetAttribute上的字典
	 */
	public void updateHorCWithInstances(HorCWithCF cooc, ArrayList<Instance> insts,
			ArrayList<String> dictionary) {
	
		ArrayList<String> utilizedAttributes = new ArrayList<String>();
		utilizedAttributes = cooc.leveragedAttributes;
		String targetAttribute = cooc.targetAttribute;
		System.out.println("update confidence of :"+utilizedAttributes.toString()+"-->>>"+targetAttribute);
		//部分expansionTerms事先已经获取，直接读取，如果没有的话，则从磁盘读取
		ArrayList<String> expansionTerms = this.readExpansionTerms(utilizedAttributes, targetAttribute, 5);
		System.out.println("Read expansion terms from file"+expansionTerms);
	if (expansionTerms.isEmpty())
		{
			System.out.println("Learning expansion terms for " + utilizedAttributes.toString()+targetAttribute);
			//如果没有现有的expansionTerms，则只能生成通过解析返回的网页生成expansionTerms
		
			expansionTerms = genAuxiliaryInformation(insts);
		}
		
		//将多余的expansion terms去掉
		while(expansionTerms.size() > termsToUse){
			
			expansionTerms.remove(expansionTerms.size()-1);
		}
		
		float  correctConfSum = 0.f;
		float  errorConfSum = 0.f;
		int correctNumSum=0, errorNumSum=0;
		ArrayList<String> leveragedValues = new ArrayList<String>();
		for (int i = 0; i < insts.size(); i ++)
		{
			System.out.println("beginning******************************************************\n"
					+ "target values for this instance is:"+insts.get(i).getTargetValue());
	
			leveragedValues.addAll(insts.get(i).getLeveragedValues());
			String searchResponse = Fetcher.graspPosQueryWeb(insts.get(i).getLeveragedValues());
			ArrayList<String> snippets = Extractor.extractAbstracts(insts.get(i).getLeveragedValues(), searchResponse);
			//下面使用Expansion Terms来做查询
			ArrayList<String> leveragedValuesWithExpansionTerm=new ArrayList<String>();
			for(int j = 0; j < expansionTerms.size();j++){
				leveragedValuesWithExpansionTerm.addAll(leveragedValues);
				leveragedValuesWithExpansionTerm.add(expansionTerms.get(j));
				String searchResponseWithExpansion = Fetcher.graspPosQueryWeb(leveragedValuesWithExpansionTerm);
				snippets.addAll(Extractor.extractAbstracts(insts.get(i).getLeveragedValues(), searchResponseWithExpansion));
				leveragedValuesWithExpansionTerm.clear();
			}
		
			String matched ="";
		//	System.out.println("before matching :::: dictionary are:"+dictionary);
			
			matched = Extractor.extractValueFromSnippets(classifier,snippets,insts.get(i).getTargetAttribute(),dictionary);//但是这个实际上没有使用到expansionTerm
			System.out.println("找到的关键词为："+matched);
			if(!matched.isEmpty()){
				cooc.coverIndexes.add(insts.get(i).tupleId);
		//		System.out.println("matches for "+insts.get(i).getTargetValue()+"are as follows#############"+matched);
				if(matched.equalsIgnoreCase(insts.get(i).getTargetValue())){
					correctNumSum++;
					System.out.println("匹配");
				}
				else
				{
					System.out.println("没有匹配");
					errorNumSum++;
				}
			}
			leveragedValues.clear();
		}
		
		cooc.coverage = (float) cooc.coverIndexes.size() / insts.size();
		System.out.println("covered index size:"+cooc.coverIndexes.size()+"\ninsts size:"+insts.size()+"\ncoverage of HorCWithCF is"+cooc.coverage);
		if( cooc.coverIndexes.size() != 0){
			System.out.println("correctNumSum"+correctNumSum);
			cooc.confidence =(float) correctNumSum / cooc.coverIndexes.size();
		}
		else{
			cooc.confidence =0f;
		}
		
		System.out.println("HorCWithCF conficence:"+cooc.confidence);
	}
	/** 对于已经捕获的expansionterms，则直接读取该expansion terms。expansion term的存储形式如下：
	 * /res/learned-expansionTerms/leveragedAttributes##targetAttribute.txt
	 * @param leveragedIndexes ArrayList<String>
	 * @param targetAttribute String
	 * @param usedTopTermNum int
	 * @return expansionTerms ArrayList<String>
	 */
	public ArrayList<String> readExpansionTerms(ArrayList<String> leveragedAttributes,
			String targetAttribute, int usedTopTermNum) {
		ArrayList<String> topSeveralTerms = new ArrayList<String>();
		
		String fileName ="";
		fileName = leveragedAttributes.toString();
		
		fileName = fileName + "##" + targetAttribute;
		String expansionTermsPath;
		expansionTermsPath =tempPath+"res/learned-expansionTerms/"+ fileName + ".txt";
		File f = new File(expansionTermsPath);
		if (f.exists() == false) {
			return topSeveralTerms;
		}
		
		ArrayList<String> allTerms = ReadWriteTxt.readFileintoArray(
				expansionTermsPath);
		
		
		for (int i = 0; i < allTerms.size() && i < usedTopTermNum; i ++) {
			topSeveralTerms.add(allTerms.get(i));
		}
		
		return topSeveralTerms;
	}
	
	/**保存生成的辅助信息
	 * @param leveragedAttributes
	 * @param targetAttribute
	 * @param snippets
	 */
	public void saveExpansionTerms(ArrayList<String> leveragedAttributes,String targetAttribute,ArrayList<String> snippets) {
		
		String fileName ="";
		fileName = leveragedAttributes.toString();
		fileName = fileName + "##" + targetAttribute;
		String expansionPath =tempPath+"res/learned-expansionTerms/"+ fileName + ".txt";
		File file = new File(expansionPath);
		try
		{
			file.createNewFile();
			ReadWriteTxt.writeArraytoFile(expansionPath, snippets);
			
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
