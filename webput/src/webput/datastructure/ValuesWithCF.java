package webput.datastructure;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class ValuesWithCF {
	public Map<String,String> values; 
	public Map<String,Float> confidences;
	
	public ValuesWithCF(){
		values = new LinkedHashMap<String,String>();
		confidences = new LinkedHashMap<String,Float>();
	}
	public ValuesWithCF(Map<String,String> values,Map<String,Float> confidences){
		values = new LinkedHashMap<String,String>();
		values.putAll(values);
		confidences = new LinkedHashMap<String,Float>();
		confidences.putAll(confidences);
	}
	public ValuesWithCF(ValuesWithCF another){
		this.values.putAll(another.values);
		this.confidences.putAll(another.confidences);
	}
	/*选择法按照confidences的大小，按照降序的顺序排列values和confidences
	public void sortValuesWithconfidences(){
		int maxconfidencesIndex;
		String tempValue;
		float tempconfidences;
		for(int i = 0; i < confidences.size();i++)
		{
			maxconfidencesIndex = i;
			for(int j = i+1;j < confidences.size();j++)
			{
				if(confidences.get(j)>confidences.get(maxconfidencesIndex))
				{
					maxconfidencesIndex = j;
				}
			}
			if(maxconfidencesIndex != i)
			{
				tempValue = values.get(i);
				values.set(i, values.get(maxconfidencesIndex));
				values.set(i,tempValue);
				
				tempconfidences = confidences.get(i);
				confidences.set(i, confidences.get(maxconfidencesIndex));
				confidences.set(i, tempconfidences);
			}
		}
	}*/
	public boolean equals(ValuesWithCF anotherOne) {
		if(this.values.equals(anotherOne.values) && this.confidences == anotherOne.confidences){
			return true;
		}
		return false;
	}
}
