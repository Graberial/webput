package webput.datastructure;

import java.util.ArrayList;

public class CHorCWithCF extends HorCWithCF{

	public CHorCWithCF(ArrayList<String> leveragedAttributes,
			String targetAttribute) {
		super(leveragedAttributes, targetAttribute);
		this.type = 2;
	}
	public CHorCWithCF(){
		super();
	}
	public CHorCWithCF(HorCWithCF inputOne) {
		super(inputOne);
	}
	
	
	public String toString() {
		String output = new String("[");
		for (int i = 0; i < this.leveragedAttributes.size()-1; i ++) {          
			output = output.concat(this.leveragedAttributes.get(i) + ", ");
		}
		output = output.concat(this.leveragedAttributes.get(this.leveragedAttributes.size()-1) + "]->");
		
		output = output.concat(this.targetAttribute + ": " + confidence + " # ");
		output = output.concat(this.coverIndexes.toString());
		
		return output;
	}
	
}
