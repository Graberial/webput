package webput.datastructure;

import java.io.Serializable;
import java.util.ArrayList;

public class HorCWithCF implements Serializable{
	public ArrayList<String> leveragedAttributes;
	public String targetAttribute;
	public float confidence;
	public float coverage;
	public ArrayList<Integer> coverIndexes;//covered indexes of tuples
	
	public int type;//1 - PHorC, 2 - CHorC
	
	public HorCWithCF(HorCWithCF anotherOne) {
		this.leveragedAttributes = new ArrayList<String>();
		this.leveragedAttributes.addAll(anotherOne.leveragedAttributes);
		this.targetAttribute = anotherOne.targetAttribute;
		
		this.confidence = anotherOne.confidence;
		this.coverage = anotherOne.coverage;
		this.coverIndexes = new ArrayList<Integer>();
		this.coverIndexes.addAll(anotherOne.coverIndexes);
	
		this.type = anotherOne.type;
	}
	
	
	public HorCWithCF(ArrayList<String> leveragedAttributes, String targetAttribute) {
		this.leveragedAttributes = new ArrayList<String>();
		this.leveragedAttributes.addAll(leveragedAttributes);
		this.targetAttribute = targetAttribute;
		
		this.confidence = 0.f;
		this.coverage=0.f;
		this.coverIndexes = new ArrayList<Integer>();
		}
	
	
	public HorCWithCF() {
		this.leveragedAttributes = new ArrayList<String>();
		this.targetAttribute = null;	
		this.confidence = 0.f;
		this.coverage=0.f;
		this.coverIndexes = new ArrayList<Integer>();
	
	}
	
	public void addNewCoverIndex(int tupleIndex){
		coverIndexes.add(tupleIndex);
	}
	public void addNewSupportIndex(int tupleIndex){
		
	}
	public void addNewCoverIndexes(ArrayList<Integer> newCoverIndexes, 
			ArrayList<Float> confOfNewCoverIndexes, 
			ArrayList<Boolean> newSupportOrNots) {
		for (int i = 0; i < newCoverIndexes.size(); i ++) {
			if (!coverIndexes.contains(newCoverIndexes.get(i))) {
				coverIndexes.add(newCoverIndexes.get(i));
						}
		}
	}
	
	public void addNewCoverIndexes(ArrayList<Integer> newCoverIndexes) {
		for (int i = 0; i < newCoverIndexes.size(); i ++) {
			if (!coverIndexes.contains(newCoverIndexes.get(i))) {
				coverIndexes.add(newCoverIndexes.get(i));
			}
		}
	}
	public boolean equals(HorCWithCF anotherOne){
		if(this.type == anotherOne.type && this.confidence == anotherOne.confidence && this.coverage == anotherOne.coverage){
			if(this.leveragedAttributes.containsAll(anotherOne.leveragedAttributes) && anotherOne.leveragedAttributes.containsAll(this.leveragedAttributes)){
				if(this.targetAttribute.equals(anotherOne.targetAttribute)){
				  return true;
				}
			}
		}
		return false;
	}
}
