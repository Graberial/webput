package webput.datastructure;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;

import com.leeonJava.fileDispose.ReadWriteObj;

public class HorCWithCFStore implements Serializable{
	
	public ArrayList<HorCWithCF>  allHorCWithCF;//all the HorC operations we have
	public ArrayList<ArrayList<Integer>> allInitCompletTids;
	public int processedHorCTimes;
	private static Logger logger = LogManager.getLogger();
	public HorCWithCFStore() {
		this.allHorCWithCF = new ArrayList<HorCWithCF>();
		this.allInitCompletTids = new ArrayList<ArrayList<Integer>>();
		this.processedHorCTimes = 0;
	}
	
	
	public boolean loadFromFile(String objPath) {
		File file = new File(objPath);
		if (file.exists()) {
			Object obj = null;
			try {
				ObjectInputStream in = new ObjectInputStream(
						new BufferedInputStream(new FileInputStream(file)));
				obj = in.readObject();
				in.close();
				HorCWithCFStore myOne = (HorCWithCFStore) obj;
						
				this.allHorCWithCF = myOne.allHorCWithCF;
				this.allInitCompletTids = myOne.allInitCompletTids;
				this.processedHorCTimes = myOne.processedHorCTimes;
				return true;
			} catch (Exception e) {
				System.err.println(objPath);
				e.printStackTrace();
			}
		}
		return false;
	}
	
	
	public void saveIntoFile(String filePath) {
		HorCWithCFStore myOne = new HorCWithCFStore();
		myOne.allHorCWithCF.addAll(this.allHorCWithCF);
		myOne.allInitCompletTids.addAll(this.allInitCompletTids);
		myOne.processedHorCTimes = this.processedHorCTimes;
	
		try {
			ObjectOutputStream out = new ObjectOutputStream(
					new BufferedOutputStream(new FileOutputStream(filePath)));
			out.writeObject(myOne);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public ArrayList<HorCWithCF> getHorCWithFCs(String leveragedValue, String targetAttribute) {
		ArrayList<HorCWithCF> pHorCs = new ArrayList<HorCWithCF>();
		for (int i = 0; i < this.allHorCWithCF.size(); i ++) {
			if (this.allHorCWithCF.get(i).targetAttribute == targetAttribute &&
					this.allHorCWithCF.get(i).leveragedAttributes.contains(leveragedValue)) {
				pHorCs.add(this.allHorCWithCF.get(i));
			}
		}
		return pHorCs;
	}
	
	public float getConfidenceForHorC(ArrayList<String> leveragedAttributes,String targetAttribute){
		for (int i = 0; i < this.allHorCWithCF.size(); i ++) {
			if (this.allHorCWithCF.get(i).targetAttribute.equals(targetAttribute))
			{
				if(allHorCWithCF.get(i).leveragedAttributes.containsAll(leveragedAttributes) &&
					leveragedAttributes.containsAll(allHorCWithCF.get(i).leveragedAttributes))
					{
						return this.allHorCWithCF.get(i).confidence;
					}
			}
		}
		System.out.println(leveragedAttributes+"->"+targetAttribute+" doesn't exist in HorCWithCFStore!!");
		return 0f;
	}
	public HorCWithCF getHorCWithFC(ArrayList<String> leveragedAttributes, String targetAttribute) {
		
		for (int i = 0; i < this.allHorCWithCF.size(); i ++) {
			if (this.allHorCWithCF.get(i).targetAttribute == targetAttribute)
				if(this.allHorCWithCF.get(i).leveragedAttributes.containsAll(leveragedAttributes) &&
						leveragedAttributes.containsAll(this.allHorCWithCF.get(i).leveragedAttributes))
				{
					System.out.println("HorC____________________________________________________");
					HorCWithCF horc = new HorCWithCF(this.allHorCWithCF.get(i)); 
					return horc;
			}
		}
		return null;
	}
	
	public HorCWithCF getBestHorCWithCF(ArrayList<String> leveragedAttributes,String targetAttribute){
		HorCWithCF bestHorC = new HorCWithCF();
		bestHorC.confidence = -1f;
		for(int i =0; i < allHorCWithCF.size();i++){
			if(targetAttribute.equals(allHorCWithCF.get(i).targetAttribute)){
				if(leveragedAttributes.containsAll(allHorCWithCF.get(i).leveragedAttributes)){
					if(allHorCWithCF.get(i).confidence >bestHorC.confidence){
						bestHorC = allHorCWithCF.get(i);
					}
				}
			}
		}
		return bestHorC;
	}
	
	/**计算HorCWithCFStore中的HorCWithCF个数
	 * @return 
	 */
	public int size(){
		return allHorCWithCF.size();
	}
	
	public HorCWithCF get(int index){
		
		if(index >= allHorCWithCF.size()){
			System.err.println("index out of bound in HorCWithCFStore");
			return null;
		}
		else
		{
			return allHorCWithCF.get(index);
		}
	
	}
	
	/**增加一个HorCWithCF
	 * @param pattern
	 */
	public void addNewHorCWithCF(HorCWithCF pattern) {
		if(!allHorCWithCF.contains(pattern))
		{
			allHorCWithCF.add(pattern);
			logger.trace(pattern);
		}
		
	}
	
	/**增加一串的pattern
	 * @param patterns ArrayList <CHorCWithCF>
	 */
	public void addAllNewHorCWithCF(ArrayList<HorCWithCF> patterns){
		for(int i=0; i< patterns.size();i++)
		{
			if(!this.contains(patterns.get(i))){
				logger.trace(patterns.get(i));
				//System.out.println(patterns.get(i).leveragedAttributes+"-->"+patterns.get(i).targetAttribute);
				allHorCWithCF.add(patterns.get(i));
			}
		}	
	}
	
	public void addNewHorCWithCF(HorCWithCF pattern, ArrayList<Integer> initCompleteTids) {
		this.allHorCWithCF.add(pattern);
		this.allInitCompletTids.add(initCompleteTids);
	}
	public boolean contains(HorCWithCF pattern){
		for(int i=0;i < allHorCWithCF.size();i++){
			if(allHorCWithCF.get(i).type ==pattern.type)
			{
				if(allHorCWithCF.get(i).leveragedAttributes.containsAll(pattern.leveragedAttributes) && 
						pattern.leveragedAttributes.containsAll(allHorCWithCF.get(i).leveragedAttributes) &&
						allHorCWithCF.get(i).targetAttribute.equals(pattern.targetAttribute)){
					return true;
				}
			}
		}
		return false;
	}
	public String toString(){
		String toStr = "";
		for(int i = 0; i < allHorCWithCF.size();i++){
			toStr.concat(allHorCWithCF.get(i).leveragedAttributes+"-->"+allHorCWithCF.get(i).targetAttribute+": "+allHorCWithCF.get(i).confidence);
		}
		return toStr;
	}
}
	
