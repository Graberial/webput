package extractor.auxiliary;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import extractor.base.*;
import fetcher.queries.*;

import java.util.ArrayList;  
import java.util.Collections;  
import java.util.Comparator;  
import java.util.HashMap;  
import java.util.Map;  
import java.util.Map.Entry;

import com.leeonJava.fileDispose.ReadWriteTxt;
import com.leeonJava.textDispose.MatchRegexinFile;

public class ExpansionTerms {
	 public static String tempPath ="C:/Users/Graberial/workspace/webput/WebContent/WEB-INF/classes/";
	public static ArrayList<String> genExpansionTermsWithGroups(SnippetsStore mySnippetStore_pos, ArrayList<Instance> insts, float threshold) {
		//0. prepare all terms
		ArrayList<String> allTerms = new ArrayList<String>();
		ArrayList<Integer> allFreqs = new ArrayList<Integer>();//sum of freqs
		ArrayList<Integer> allDistFreqs = new ArrayList<Integer>();//freqs for each distinct query
		
		//gen allwords
		ArrayList<String> tempWordsOfDistQuery = new ArrayList<String>();
		ArrayList<Integer> tempFreqsOfDistQuery = new ArrayList<Integer>();//sum of freqs
		
		for (int i = 0; i < mySnippetStore_pos.allSnippets.size(); i ++) {
			
			System.out.println("Processing " + i);
		//	System.out.println(mySnippetStore_pos.allSnippets.get(i).snippet);
			
			ArrayList<String> tempWords = MatchRegexinFile.getAllMatch("[\\w]+", mySnippetStore_pos.allSnippets.get(i).title.concat(" " + mySnippetStore_pos.allSnippets.get(i).snippet));
		
			if (i == 0 || (mySnippetStore_pos.allSnippets.get(i).leveragedValues.containsAll(mySnippetStore_pos.allSnippets.get(i-1).leveragedValues) &&
					mySnippetStore_pos.allSnippets.get(i-1).leveragedValues.containsAll(mySnippetStore_pos.allSnippets.get(i).leveragedValues))) {
				//下面的找出其中一个snippets中单词的出现次数
				for (int j = 0; j < tempWords.size(); j ++) {
					int index = tempWordsOfDistQuery.indexOf(tempWords.get(j));
					if (index == -1) {
						tempWordsOfDistQuery.add(tempWords.get(j));
						tempFreqsOfDistQuery.add(1);
						allDistFreqs.add(1);
					}
					else {
						tempFreqsOfDistQuery.set(index, tempFreqsOfDistQuery.get(index)+1);
					}
				}
//				System.out.println(tempWordsOfDistQuery);
//				System.out.println(tempFreqsOfDistQuery);
			}
			//不是第一个snippets，和上一个也不一样
			else {
				for (int j = 0; j < tempWordsOfDistQuery.size(); j ++) {
					int index = allTerms.indexOf(tempWordsOfDistQuery.get(j));
					if (index == -1) {
						allTerms.add(tempWordsOfDistQuery.get(j));
						allFreqs.add(tempFreqsOfDistQuery.get(j));
						allDistFreqs.add(1);
					}
					else {
						allFreqs.set(index, allFreqs.get(index)+tempFreqsOfDistQuery.get(j));
						allDistFreqs.set(index, allDistFreqs.get(index)+1);
					}
				}

				//clear the temp arrays and re-use it
				tempWordsOfDistQuery.clear();
				tempFreqsOfDistQuery.clear();
				
				for (int j = 0; j < tempWords.size(); j ++) {
					int index = tempWordsOfDistQuery.indexOf(tempWords.get(j));
					if (index == -1) {
						tempWordsOfDistQuery.add(tempWords.get(j));
						tempFreqsOfDistQuery.add(1);
					}
					else {
						tempFreqsOfDistQuery.set(index, tempFreqsOfDistQuery.get(index)+1);
					}
				}
			}
		}
		System.out.println("before remove low frequent words"+allTerms);
		System.out.println("before remove low frequent words"+allFreqs);
		
		ArrayList<String> stopwords = ReadWriteTxt.readFileintoArray(tempPath+"res/commonstopwords(eng).txt");
		
		stopwords.add("org");   stopwords.add("edu"); stopwords.add("Foster");stopwords.add("uk");stopwords.add("ac");stopwords.add("le"); 
		stopwords.add("www");stopwords.add("http");
		for (int i = 0; i < allFreqs.size(); i ++) {
			//删除数字
			if (allTerms.get(i).matches("\\d+")) {
				allFreqs.remove(i);
				allDistFreqs.remove(i);
				allTerms.remove(i);
				i --;
			}
			//词长度小于2
			else if (allTerms.get(i).length() < 2) {
				allFreqs.remove(i);
				allDistFreqs.remove(i);
				allTerms.remove(i);
				i --;
			}
			
			
			//手工删除一些不相关的词
			else if (stopwords.contains(allTerms.get(i).toLowerCase())) {
				allFreqs.remove(i);
				allDistFreqs.remove(i);
				allTerms.remove(i);
				i --;
			}
			
			//如果这些词在查询60%的查询结果中都出现，算作有效的词
			else if (allFreqs.get(i) < 0.6*insts.size()) { 
				allFreqs.remove(i);
				allDistFreqs.remove(i);
				allTerms.remove(i);
				i --;
			}
			
		}
		System.out.println("After remove all low frequent word, all terms are:\n"+allTerms);
		System.out.println("word frequence:\n"+allFreqs);
		//按照词频排序
		Map sortExpansions = new HashMap<String,Integer>();
		
		for(int t = 0; t <allTerms.size();t++){
			sortExpansions.put(allTerms.get(t),allFreqs.get(t));
		}
		
		List<Map.Entry<String,Integer>> arraylist = new ArrayList<Map.Entry<String, Integer>>(sortExpansions.entrySet());
		
		Collections.sort(arraylist,new Comparator<Map.Entry<String, Integer>>(){
			public int compare(Entry<String,Integer> o1,Entry<String,Integer>o2){
				return o2.getValue()- o1.getValue();
			}
		});
		
		ArrayList<String> terms = new ArrayList<String>();
		for(int i = 0; i < arraylist.size() && i < insts.size();i++){
			terms.add(arraylist.get(i).getKey());
		}
		
		System.out.println(arraylist);
		return terms;
	}
	
	
}
