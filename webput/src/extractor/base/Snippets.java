package extractor.base;

import java.util.ArrayList;
import java.util.HashSet;
import com.leeonJava.internet.WebFormatter;
import com.leeonJava.textDispose.MatchRegexinFile;

public class Snippets {
	public ArrayList<String> leveragedValues;
	public int rank;
//	public boolean posOrNeg;
	public String url;
	public String title;
	public String snippet;
	
	public Snippets() {
		this.leveragedValues = new ArrayList<String>();
		this.rank = -1;
		this.url = new String();
		this.title = new String();
		this.snippet = new String();
	}

	public Snippets(Integer rank, String url) {
		this.leveragedValues = new ArrayList<String>();
		this.rank = rank;
		this.url = new String(url);
	}

	/**
	 * @param leveragedValues
	 * @param url
	 * @param title
	 * @param snippet
	 * @param rank
	 */
	public Snippets(ArrayList<String> leveragedValues,String url,String title,String snippet,int rank){
		this.leveragedValues = new ArrayList<String>();
		this.leveragedValues.addAll(leveragedValues);
		this.url = new String(url);
		this.title= new String(title);
		this.snippet = new String(snippet);
		this.rank = rank;
	}
	public void genSnippetInfo() {
		
		ArrayList<String> snippetArray = MatchRegexinFile.getAllMatch("[\\w]+", WebFormatter.html2text(snippet));
		ArrayList<String> titleArray = MatchRegexinFile.getAllMatch("[\\w]+", WebFormatter.html2text(title));
		this.snippet = new String("");
		if (snippetArray != null && snippetArray.size() != 0) {
			for (int i = 0; i < snippetArray.size()-1; i ++) {
				snippet = snippet.concat(snippetArray.get(i)+" ");
			}
			snippet = snippet.concat(snippetArray.get(snippetArray.size()-1));
		}
		
		this.title = new String("");
		if (titleArray != null && titleArray.size() != 0) {
			for (int i = 0; i < titleArray.size()-1; i ++) {
				title = title.concat(titleArray.get(i)+" ");
			}
			title = title.concat(titleArray.get(titleArray.size()-1));
		}
	}
	
	
	public boolean isEqual(Snippets anotherOne) {
		if (this.leveragedValues.equals(anotherOne.leveragedValues) && this.rank == anotherOne.rank) {
			return true;
		}
		return false;
	}
	
	
	public boolean isEqual(ArrayList<String> leveragedValues, int rank) {
		if (this.leveragedValues.containsAll(leveragedValues) && leveragedValues.containsAll(this.leveragedValues) && this.rank == rank) {
			return true;
		}
		return false;
	}	
}
