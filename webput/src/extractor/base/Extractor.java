package extractor.base;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.sequences.DocumentReaderAndWriter;
import edu.stanford.nlp.util.CoreMap;
import fetcher.implementation.Fetcher;
import fetcher.queries.*;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;
import java.util.Properties;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.leeonJava.fileDispose.ReadWriteTxt;
import com.leeonJava.internet.EnDeUrl;
import com.leeonJava.internet.HtmlofUrl;
import com.leeonJava.textDispose.MatchRegexinFile;

import setExpansion.functions.FetchHtmlTemp;
import webput.datastructure.ValuesWithCF;


public class Extractor{

	//*****following methods use Bing Api 2.0 to get results from search engine
	
	/**由Instance抓取的网页以及使用到的Instance值，解析出所有的Snippets
	 * @param inst
	 * @param sJResult
	 * @return
	 */
	public static ArrayList<Snippets> extractSnippets(ArrayList<String> leveragedValues,String searchResponse) {
		ArrayList<Snippets> mySnippets = new ArrayList<Snippets>();
		JSONArray jsonArray = null;
		JSONObject jsonSearchResponse = null;
		JSONObject jsonWeb = null;
//		System.out.println(htmlpage);
		try {
			JSONObject json = new JSONObject(searchResponse);
		
			jsonSearchResponse = json.getJSONObject("d");
		
			jsonArray = jsonSearchResponse.getJSONArray("results");
		
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
		for(int i=0; i < jsonArray.length();i++)
		{
			try{
				String url  = jsonArray.getJSONObject(i).getString("Url");
				String title = jsonArray.getJSONObject(i).getString("Title");
				String description = jsonArray.getJSONObject(i).getString("Description");
				
				//以上是分离出JSONArray中的第i个结果，下面判断第i个结果是不是包含查询的所有关键词
				//检查查到的这些动态简介是不是有效的，即是否包含使用到的关键字，这个比较严格，如果后面有问题需要放宽
				boolean valid = true;
				for(int k = 0; k < leveragedValues.size();k++){
					if(description.contains(leveragedValues.get(k)))
					{
						valid = false;
						break;
					}
				}
				if(valid){
					Snippets validSnippets = new Snippets(leveragedValues,url,title,description,i);
					mySnippets.add(validSnippets);
				}
				
			}catch(JSONException e){
				System.out.println("Get Objects from JSONArray error in Extractor.extractSnippetsFromJSON");
				e.printStackTrace();
			}
		}
		
		return mySnippets;
		//	public Snippets(ArrayList<String> leveragedValues,String url,String title,String snippet,int rank){
	}
	
	
	public static ArrayList<String> extractAbstracts(ArrayList<String> leveragedValues,String searchResponse) {
		ArrayList<String> mySnippets = new ArrayList<String>();
		JSONArray jsonArray = null;
		JSONObject jsonSearchResponse = null;
		JSONObject jsonWeb = null;
		//System.out.println(searchResponse);
		try {
			JSONObject json = new JSONObject(searchResponse);
		
			jsonSearchResponse = json.getJSONObject("d");
		
			jsonArray = jsonSearchResponse.getJSONArray("results");
		
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
		for(int i=0; i < jsonArray.length();i++)
		{
			try{
			//	String url  = jsonArray.getJSONObject(i).getString("Url");
				String title = jsonArray.getJSONObject(i).getString("Title");
				String description = jsonArray.getJSONObject(i).getString("Description");
				//检查查到的这些动态简介是不是有效的，即是否包含使用到的关键字，这个比较严格，如果后面有问题需要放宽
				boolean valid = false;
				for(int k = 0; k < leveragedValues.size();k++){
					if(description.contains(leveragedValues.get(k)))
					{
						valid = true;
						break;
					}
				}
				if(valid){
					String validSnippets = title+description;
					mySnippets.add(validSnippets);
				}
			//	String validSnippets = title+description;
     		//	mySnippets.add(validSnippets);
			}catch(JSONException e){
				System.out.println("Get Objects from JSONArray error in Extractor.extractAbstract");
				e.printStackTrace();
			}
		}
		
		return mySnippets;
		//	public Snippets(ArrayList<String> leveragedValues,String url,String title,String snippet,int rank){
	}
	

	/**最简单的查找关键词的方法，根据给定的词典，将出现次数多的当做要找的关键词
	 * 应该使用expansionTerms来抽取关键词的，这里仅仅使用字典的方式来抽取
	 * @param snippets      一些事先解析出来的搜索片段
	 * @param Dictionary   包含带解析关键字的词典
	 * @return                    可能的带查找关键词以及其可信度
	 */
	public  static String extractValueFromSnippets(AbstractSequenceClassifier<CoreLabel> classifier, 
			ArrayList<String> snippets,String entityType,ArrayList<String> dictionary) {
		System.out.println(entityType);
		//将每个snippets和dictionary比对，记录dictionary的每条记录在snippets中出现的次数
		if(entityType.equals("TITLE") || entityType.equals("COUNTRY") || entityType.equals("ORGANIZATION"))
		{
			ArrayList<Integer> scoreOfDicEntries = new ArrayList<Integer>();
			scoreOfDicEntries.add(0);
			for (int dicId = 1; dicId < dictionary.size(); dicId ++) {
				String dicEntry = dictionary.get(dicId);
				int tempScore = 0;
				for (int docId = 0; docId < snippets.size(); docId ++) {
					//System.out.println(snippets.get(docId));
					if(snippets.get(docId).contains(dicEntry)) {
						System.out.println("从文档中找到目标关键词"+dicEntry);
						tempScore ++;
					}	
	//				else
	//				{
	//					System.out.println("不包含");
	//				}
				}
			//	System.out.println(dicEntry+tempScore+" times");
				scoreOfDicEntries.add(tempScore);
			}
				
			//在字典中出现次数最多的词，认为是需要抽取的关键词
			int maxIndex=0;
			for(int dicId = 1; dicId < scoreOfDicEntries.size();dicId++){
				if(scoreOfDicEntries.get(dicId) > 0)
				{
					if(scoreOfDicEntries.get(dicId) >= scoreOfDicEntries.get(maxIndex)){
						maxIndex = dicId;
					}
				}
			}
			
			if(scoreOfDicEntries.get(maxIndex) ==0)
				return "";
			else
			{
				return dictionary.get(maxIndex);
			}
				
		}
		else{ //NER  NAME EMAIL
			Map<String,Integer> values = new LinkedHashMap<String,Integer>();
			
			if(entityType.equals("NAME")){	// beginning of matching name
				System.out.println("Finding personName ------------------------------------------------");
				String sentenceWithInlineXML="";
				for(String snippet:snippets){
					sentenceWithInlineXML = classifier.classifyWithInlineXML(snippet);//"[\\w]+"
					
					ArrayList<String>  allMatches = MatchRegexinFile.getAllMatch("(<PERSON.*?>)(.+?)(</PERSON>)",sentenceWithInlineXML);
					for(int i =0; i < allMatches.size();i++){
						String personName = allMatches.get(i).replaceAll("(<PERSON.*?>)(.+?)(</PERSON>)", "$2");
						System.out.println(personName);
						if(!values.containsKey(personName)){
							values.put(personName,1);
						}
						else{
							values.put(personName,values.get(personName)+1);
						}	
					}
				}
				
			}//end of name
			else{
			
				/*if(entityType.equals("ORGANIZATION"))
				{
					String sentenceWithInlineXML="";
					for(String snippet:snippets){
						sentenceWithInlineXML = classifier.classifyWithInlineXML(snippet);//"[\\w]+"
						
						ArrayList<String>  allMatches = MatchRegexinFile.getAllMatch("(<ORGANIZATION.*?>)(.+?)(</ORGANIZATION>)",sentenceWithInlineXML);
	
						for(int i =0; i < allMatches.size();i++){
							String organization = allMatches.get(i).replaceAll("(<ORGANIZATION.*?>)(.+?)(</ORGANIZATION>)", "$2");
							if(!values.containsKey(organization)){
								values.put(organization,1);
							}
							else{
								values.put(organization,values.get(organization)+1);
							}	
						}
					}
				}*/
			//	else{
					ArrayList<String> emails = new ArrayList<String>();
				//String regx = "[a-zA-Z0-9_-]+@\\w+\\.[a-z]+(\\.[a-z]+)?";  
					String regx = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
					for(String snippet:snippets){
						emails = MatchRegexinFile.getAllMatch(regx,snippet);
    					System.out.println(emails);
						for(int i =0; i < emails.size();i++){
							if(!values.containsKey(emails.get(i))){
								values.put(emails.get(i),1);
							}
							else{
								values.put(emails.get(i),values.get(emails.get(i))+1);
							}	
						}
					}
				//}
		}
		if(!values.isEmpty()){
			List<Map.Entry<String,Integer>> arraylist = new ArrayList<Map.Entry<String, Integer>>(values.entrySet());
			
			Collections.sort(arraylist,new Comparator<Map.Entry<String, Integer>>(){
				public int compare(Entry<String,Integer> o1,Entry<String,Integer>o2){
					return o2.getValue()- o1.getValue();
				}
			});
			return arraylist.get(0).getKey();
		}
		else{
			return "";
		}
	}
		
	}


	/*public static SnippetsStore fetchSnippetsForInstances(ArrayList<Instance> insts){
		SnippetsStore mySnippets = new SnippetsStore();
		Instance inst = new Instance();
		for(int i =0; i < insts.size();i++)
		{
			inst = insts.get(i);
			String fileName = constructPostFileName(inst.getLeveragedValues());
			String queryFilePath ="res/BingSearchApi/"+fileName;
			String htmlpage ="";
			//首先看由此Instance查询的网页是否已经捕获了
			File file = new File(queryFilePath);
			if(file.exists())
			{
				htmlpage = ReadWriteTxt.readFile(queryFilePath, "");
			}
			else
			{
			//	String urlstr = constructPostURL(inst,20);

				try {
						//htmlpage = getHtmlforUrl(urlstr);
						htmlpage = getBingApiResults(inst.getLeveragedValues());
						if (htmlpage == null) 
						{
							System.err.println("获取BingApiResult调用失败，没哟返回值");
						}
						else
						{
							FetchHtmlTemp.writeHtmlintoFileinTxt(htmlpage, queryFilePath);
						}	
					} catch (Exception e)
					{
						e.printStackTrace();
					}
			}
			ArrayList<Snippets> tempSnippets = Extractor.extractSnippetsFromJSON(inst, htmlpage);
			for(int k=0;k < tempSnippets.size();k++)
			{
				System.out.println(tempSnippets.get(k).snippet);
				mySnippets.add(tempSnippets.get(k));
			}
		}
		return mySnippets;
	}*/
}



