package extractor.base;
import java.util.ArrayList;



public class SnippetsStore{
	
	public ArrayList<Snippets> allSnippets;
	
	public SnippetsStore() {//int choice (we may consider how to set choice here)
		this.allSnippets = new ArrayList<Snippets>();
	}
	
	public ArrayList<String> getSnippetsForLeveragedValues(ArrayList<String> leveragedValues) {
		ArrayList<String> snippetsInRequest = new ArrayList<String>();
		for (int i = 0; i < this.allSnippets.size(); i ++) {
			if (this.allSnippets.get(i).leveragedValues.containsAll(leveragedValues) 
					&& leveragedValues.containsAll(this.allSnippets.get(i).leveragedValues)) {
				snippetsInRequest.add(this.allSnippets.get(i).title + 
						" " + this.allSnippets.get(i).snippet);
			}
		}
		return snippetsInRequest;
	}
	
	public ArrayList<String> getSnippetsForAllAttr() {
		ArrayList<String> snippetsInRequest = new ArrayList<String>();
		for (int i = 0; i < this.allSnippets.size(); i ++) {
			snippetsInRequest.add(this.allSnippets.get(i).title 
					+ " " + this.allSnippets.get(i).snippet);
		}
		return snippetsInRequest;
	}
	
	public void add(Snippets snippet)
	{
		this.allSnippets.add(snippet);
	}
	public void addAll(ArrayList<Snippets> snipppets){
		this.allSnippets.addAll(snipppets);
	}
	public void addNewRecord(ArrayList<String> leveragedValues, String url, String title, 
			String snippet, int rank) {
		Snippets newRecord = new Snippets();
		newRecord.leveragedValues.addAll(leveragedValues);
		newRecord.url = url;
		newRecord.title = title;
		newRecord.snippet = snippet;
		newRecord.rank = rank;
		this.allSnippets.add(newRecord);
	}
	
	public void addNewRecord(ArrayList<String> leveragedValues, String url, String title, 
			String snippet, int rank, boolean posOrNeg) {
		Snippets newRecord = new Snippets();
		newRecord.leveragedValues.addAll(leveragedValues);
		newRecord.url = url;
		newRecord.title = title;
		newRecord.snippet = snippet;
		newRecord.rank = rank;
	//	newRecord.posOrNeg = posOrNeg;
		this.allSnippets.add(newRecord);
	}
	
	public void removeRecords(ArrayList<String> leveragedValues) {
		for (int i = 0; i < this.allSnippets.size(); i ++) {
			if (this.allSnippets.get(i).leveragedValues.containsAll(leveragedValues) 
					&& leveragedValues.containsAll(this.allSnippets.get(i).leveragedValues)) {
				this.allSnippets.remove(i);
				i --;
			}
		}
	}
	
	public void genSnippetInfoforRecords() {
		for (int i = 0; i < allSnippets.size(); i ++) {
			if (i % 1000 == 0)
				System.out.println(i + " record info is finished!");
			allSnippets.get(i).genSnippetInfo();
		}
	}
	
	public int indexOf(ArrayList<String> leveragedValues, int rank) {
		for (int i = 0; i < allSnippets.size(); i ++) {
			if (allSnippets.get(i).leveragedValues.containsAll(leveragedValues) 
					&& leveragedValues.containsAll(allSnippets.get(i).leveragedValues)) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean contains(ArrayList<String> leveragedValues, int rank) {
		for (int i = 0; i < allSnippets.size(); i ++) {
			if (allSnippets.get(i).isEqual(leveragedValues, rank)) {
				return true;
			}
		}
		return false;
	}
	
	
}

