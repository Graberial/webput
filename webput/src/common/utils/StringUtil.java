package common.utils;

public class StringUtil {
	/**
	 * 将数据根据separator连接成字符串
	 * 
	 * @param array
	 * @param separator
	 * @return
	 */
	public static String join(Object[] array, char separator) {
		if (array == null) {
			return null;
		}
		int arraySize = array.length;
		int bufSize = (arraySize == 0 ? 0 : ((array[0] == null ? 16 : array[0]
				.toString().length()) + 1) * arraySize);
		StringBuffer buf = new StringBuffer(bufSize);

		for (int i = 0; i < arraySize; i++) {
			if (i > 0) {
				buf.append(separator);
			}
			if (array[i] != null) {
				buf.append(array[i]);
			}
		}
		return buf.toString();
	}
	
	public static String join(Object[] array, String separator) {
		if (array == null) {
			return null;
		}
		int arraySize = array.length;
		int bufSize = (arraySize == 0 ? 0 : ((array[0] == null ? 16 : array[0]
				.toString().length()) + 1) * arraySize);
		StringBuffer buf = new StringBuffer(bufSize);

		for (int i = 0; i < arraySize; i++) {
			if (i > 0) {
				buf.append(separator);
			}
			if (array[i] != null) {
				buf.append(array[i]);
			}
		}
		return buf.toString();
	}

	/**
	 * 首字母大写
	 * 
	 * @param str
	 * @return
	 */
	public static String firstCharToUpperCase(String str) {
		if (null == str) {
			return null;
		}
		if (str.isEmpty()) {
			return str;
		}
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	/**
	 * 获得两个字符串的最长相同子串
	 * 
	 * @param str1
	 * @param str2
	 * @return
	 */
	public static String getMaxSubStr(String str1, String str2) {
		String longStr = str1.length() > str2.length() ? str1 : str2;
		String shortStr = str1.equals(longStr) ? str2 : str1;
		for (int i = 0; i < shortStr.length(); i++) {
			for (int j = 0, k = shortStr.length() - i; k <= shortStr.length(); j++, k++) {
				String temp = shortStr.substring(j, k);
				if (longStr.contains(temp))
					return temp;
			}
		}
		return null;
	}
	
	/**
	 * 移除所有标点符号
	 * 
	 * @param str
	 * @return
	 */
	public static String removePunct(String str){
		if (null == str || str.isEmpty()){
			return "";
		}
		return str.replaceAll("[\\pP\\p{Punct}\\s]", " ").trim();
	}
	
	/**
	 * 移除所有 HTML/XML 标签
	 * 
	 * @param str
	 * @return
	 */
	public static String removeTages(String str){
		if (null == str || str.isEmpty()){
			return "";
		}
		return str.replaceAll("</?[^<]+>", "").trim();
	}

	/**
	 * 将字符串中连续的字符串当成单一数字对待
	 * 
	 * @param input
	 * @return
	 */
	public static String replaceNumber(String input) {
		if (input == null ) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		boolean hasreplace = false;
		for (char c : input.toCharArray()) {
			if (Character.isDigit(c)) {
				if (!hasreplace) {
					sb.append("#");
					hasreplace = true;
				}
			} else{
				sb.append(c);
				hasreplace = false;
			}
		}
		return sb.toString();
	}

}
