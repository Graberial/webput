package common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternUtil {
	public static int flags = Pattern.CASE_INSENSITIVE;
	public static final int FIRST_GROUP = 1;

	/**
	 * 获得Matcher对象
	 * 
	 * @param input
	 * @param regex
	 * @return
	 */
	public static Matcher matcher(String input, String regex) {
		Pattern pattern = Pattern.compile(regex, flags);
		return pattern.matcher(input);
	}
	
	/**
	 * 正则查找是否存在
	 * 
	 * @param input
	 * @param regex
	 * @return
	 */
	public static boolean exisit(String input, String regex) {
		Matcher matcher = matcher(input, regex);
		if (matcher.find()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 正则获取第group个匹配
	 * 
	 * @param input
	 * @param regex
	 * @param group
	 * @return
	 */
	public static String group(String input, String regex, int group) {
		Matcher matcher = matcher(input, regex);
		if (matcher.find()) {
			if (-1 == group) {
				return matcher.group();
			} else {
				return matcher.group(group);
			}
		} else {
			return null;
		}
	}

	/**
	 * 正则获取第一个匹配
	 * 
	 * @param input
	 * @param regex
	 * @return
	 */
	public static String first(String input, String regex) {
		return group(input, regex, FIRST_GROUP);
	}
	
	/**
	 * 正则获取最后一个匹配
	 * 
	 * @param input
	 * @param regex
	 * @return
	 */
	public static String last(String input, String regex){
		Matcher matcher = matcher(input, regex);
		if (matcher.find()) {
			return matcher.group(matcher.groupCount());
		}else{
			return null;
		}
	}
}