package database.dao;

import java.util.Map;

import database.annotation.TableAnnotationUtil;

/**
 * 默认的 Model
 * 
 * @author JiangJun
 */
public class DefaultModel extends Model{
	/**
	 * 默认主键
	 */
	public static final String PRIMARY_KEY = "Name";
	
	public DefaultModel(){
		this.setPrimaryKey(PRIMARY_KEY);
	}
	
	public DefaultModel(String tableName){
		this.setPrimaryKey(PRIMARY_KEY);
		this.setTableName(tableName);
	}

	@Override
	public Object getObject(Object obj, Map<String, String> map) {
		try {
			return TableAnnotationUtil.getPO(obj, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Map<String, String> getDataMap(Object obj) {
		try {
			return TableAnnotationUtil.getData(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
