package database.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import common.utils.StringUtil;
import database.connection.Config;
import database.connection.DbConnection;
import database.objects.Attribute;
import database.objects.Column;
import database.objects.Key;
import database.objects.Table;
import database.sql.DbParser;
import database.sql.DbParser.OPTIONS;

/**
 * 抽象 Model
 * 
 * @author JiangJun
 */
public abstract class Model {
	/**
	 * 是否开启日志
	 */
	public static boolean SQL_LOGGER = true;
	/**
	 * 数据库连接类
	 */
	protected DbConnection dbConnection = DbConnection.getDbConnection();
	/**
	 * 具体数据库实例类
	 */
	protected static DbParser dbParser = DbParser.getInstance();
	/**
	 * 日志
	 */
	protected static Logger logger = LogManager.getLogger();

	/**
	 * 当前数据库对象
	 */
	private Table table = null;
	/**
	 * 数据表名
	 */
	private String tableName;
	/**
	 * 数据表主键
	 */
	private String primaryKey;

	/**
	 * 执行的SQL
	 */
	private String sql;

	/**
	 * 查询结果集
	 */
	private List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();

	/**
	 * 设定增删改查的参数
	 */
	private Map<OPTIONS, String> options = new HashMap<OPTIONS, String>();

	/*----------------------------------------------------
	 * Getter/Setter
	 *--------------------------------------------------*/

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
		// 同时设置表名
		this.setTableName(table.getName());
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
		this.resetOptions();
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public List<Map<String, String>> getResultList() {
		return resultList;
	}

	public void setResultList(List<Map<String, String>> resultList) {
		this.resultList = resultList;
	}

	/**
	 *  获得持久化对象
	 *  
	 * @param obj
	 * @param map
	 * @return
	 */
	public abstract Object getObject(Object obj, Map<String, String> map);
	/**
	 * 根据对象获得map
	 * 
	 * @param obj
	 * @return
	 */
	public abstract  Map<String, String> getDataMap(Object obj);
	
	/*----------------------------------------------------
	 * 数据表相关操作
	 *--------------------------------------------------*/

	/**
	 * 删除当前表
	 * 
	 * @return
	 */
	public boolean drop() {
		sql = dbParser.dropTable(tableName);
		return this.execute(sql);
	}

	/**
	 * 删除后新增当前表
	 * 
	 * @return
	 */
	public boolean dropAndCreate() {
		this.drop();
		return this.create();
	}

	/**
	 * 创建数据表
	 * 
	 * @return
	 */
	public boolean create() {
		// 必须设置表结构
		if (null == table) {
			logger.error("必须先设置表结构！");
			return false;
		}

		sql = dbParser.createTable(table);
		return this.execute(sql);
	}

	/**
	 * 获得当前表信息
	 * 
	 * @return
	 */
	public Table table() {
		sql = dbParser.showTable(tableName, Config.JDBC_DATABASE);
		this.log();

		ResultSet rs = dbConnection.executeQuery(sql);
		try {
			List<Map<String, String>> tableList = this.resultSet2List(rs);
			if (tableList.isEmpty()) {
				return null;
			} else {
				Map<String, String> map = tableList.get(0);
				table = new Table();
				table = dbParser.map2Table(map);
				table.setColumns(columns());
				table.setKeys(keys());
			}
		} catch (SQLException e) {
			logger.error("SQL 错误：" + e.toString());
		}
		return table;
	}

	/**
	 * 获得所有列
	 * 
	 * @return
	 */
	public Set<Column<?>> columns() {
		sql = dbParser.showColumns(tableName);
		this.log();

		ResultSet rs = dbConnection.executeQuery(sql);
		try {
			List<Map<String, String>> columnsList = this.resultSet2List(rs);
			Set<Column<?>> columns = new LinkedHashSet<Column<?>>();
			for (Map<String, String> map : columnsList) {
				columns.add(dbParser.map2Column(map));
			}
			return columns;
		} catch (SQLException e) {
			logger.error("SQL 错误：" + e.toString());
		}
		return null;
	}

	/**
	 * 获得所有索引
	 * 
	 * @return
	 */
	public Set<Key> keys() {
		sql = dbParser.showKeys(tableName);
		this.log();
		ResultSet rs = dbConnection.executeQuery(sql);
		try {
			List<Map<String, String>> keysList = this.resultSet2List(rs);
			Set<Key> keys = new HashSet<Key>();
			for (Map<String, String> map : keysList) {
				Key key = dbParser.map2Key(map);
				boolean exisit = false;
				for (Key tmp : keys) {
					if (tmp.getName().equals(key.getName())) {
						exisit = true;
						for (String column : key.getColumns()) {
							tmp.addColumn(column);
						}
						break;
					}
				}
				// 该索引不存在时添加
				if (!exisit) {
					keys.add(key);
				}
			}
			return keys;
		} catch (SQLException e) {
			logger.error("SQL 错误：" + e.toString());
		}
		return null;
	}

	/**
	 * 获得当前数据表的所有列
	 * 
	 * @return
	 */
	public Set<String> fields() {
		Set<String> fields = new LinkedHashSet<String>();
		Set<Column<?>> columns = columns();
		if (null != columns) {
			for (Column<?> column : columns) {
				fields.add("`" + column.getName() + "`");
			}
		}
		return fields;
	}

	/**
	 * 获得所有属性及其值出现次数
	 * 
	 * @return
	 */
	public List<Attribute> attributes() {
		return attributes(null);
	}

	/**
	 * 获得所有属性及其值出现次数
	 * 
	 * @param ignore  需要忽略的列
	 * @return
	 */
	public List<Attribute> attributes(List<String> ignore) {
		Set<String> fields = fields();
		// 移除主键元素
		if (null != primaryKey && !primaryKey.isEmpty()) {
			fields.remove("`" + primaryKey + "`");
		}
		// 移除被忽略的元素
		if (null != ignore && !ignore.isEmpty()) {
			for (String ignoreColumnName : ignore) {
				fields.remove("`" + ignoreColumnName + "`");
			}
		}
		setOption(OPTIONS.FIELD, StringUtil.join(fields.toArray(), ','));

		sql = dbParser.select(options);
		this.log();

		List<Attribute> attributes = new LinkedList<Attribute>();
		ResultSet rs = dbConnection.executeQuery(sql);

		try {
			// 设置结果集
			this.resultList = resultSet2List(rs);

			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();
			for (int i = 1; i <= cols; i++) {
				Attribute attribute = new Attribute();
				attribute.setColumnName(rsmd.getColumnName(i).toUpperCase());
				attributes.add(attribute);
			}
			
			// 游标移至开始处
			rs.first();
			do {
				for (int i = 1; i <= cols; i++) {
					String value = rs.getString(i);
					if (null == value || value.trim().isEmpty()) {
						value = null;
					}
					attributes.get(i - 1).addValue(value);
				}
			} while(rs.next());

			for (Attribute attribute : attributes) {
				attribute.sort();
			}
		} catch (SQLException e) {
			logger.error("SQL 错误：" + e.toString());
		} finally {
			this.resetOptions();
		}
		return attributes;
	}
	
	/**
	 * 获得所有属性及其值出现次数
	 * 
	 * @param ignore  需要忽略的列
	 * @return 返回 map
	 */
	public Map<String,Attribute> attributesMap(List<String> ignore) {
		List<Attribute> attributes = this.attributes();
		Map<String, Attribute> map = new LinkedHashMap<String, Attribute>();
		for (Attribute attribute : attributes) {
			map.put(attribute.getColumnName(), attribute);
		}
		return map;
	}

	/**
	 * 修改列
	 * 
	 * @param oldColumn
	 * @param newColumn
	 * @return
	 */
	public boolean alterColumn(Column<?> oldColumn, Column<?> newColumn) {
		sql = dbParser.alterColumn(tableName, oldColumn, newColumn);
		return this.execute(sql);
	}

	/**
	 * 删除列
	 * 
	 * @param columnName
	 * @return
	 */
	public boolean dropColumn(String columnName) {
		sql = dbParser.dropColumn(tableName, columnName);
		return this.execute(sql);
	}

	/**
	 * 新增列
	 * 
	 * @param column
	 * @return
	 */
	public boolean addColumn(Column<?> column) {
		sql = dbParser.addColumn(tableName, column);
		return this.execute(sql);
	}

	/**
	 * 拷贝表数据
	 * 
	 * @param newTableName
	 * @return
	 */
	public boolean copyTableStruct(String newTableName) {
		sql = dbParser.copyTableStruct(tableName, newTableName);
		return this.execute(sql);
	}

	/**
	 * 拷贝表结构及数据
	 * 
	 * @param newTableName
	 * @return
	 */
	public boolean copyTable(String newTableName) {
		if (!this.copyTableStruct(newTableName)) {
			return false;
		}
		sql = dbParser.copyTable(tableName, newTableName);
		return this.execute(sql);
	}

	/**
	 * 调用执行数据库操作的方法
	 * 
	 * @param sql
	 * @return
	 */
	protected boolean execute(String sql) {
		if (null == sql || sql.isEmpty()) {
			return false;
		}
		this.log();
		return dbConnection.execute(sql);
	}

	/*----------------------------------------------------
	 * 数据查询相关
	 *--------------------------------------------------*/
	/**
	 * 设置增删改查条件
	 * 
	 * @param where
	 * @return
	 */
	public Model where(String where) {
		if (null != where) {
			this.setOption(OPTIONS.WHERE, where);
		}
		return this;
	}

	/**
	 * 设置增删改查条件
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public Model where(String key, String value) {
		Map<String, String> array = new HashMap<String, String>();
		if (null != key) {
			array.put(key, value);
		}
		return where(array);
	}
	
	/**
	 * 设置增删改查条件
	 * 
	 * @param array
	 * @return
	 */
	public Model where(Map<String, String> array) {
		List<String> where = new ArrayList<String>();
		if (null != array && ! array.isEmpty()) {
			for (String key : array.keySet()) {
				where.add(String.format("`%s`='%s'", key, array.get(key)));
			}
		}
		this.setOption(OPTIONS.WHERE, StringUtil.join(where.toArray(), " AND "));
		return this;
	}

	/**
	 * 设置参数
	 * 
	 * @param option
	 * @param value
	 * @return
	 */
	public Model setOption(OPTIONS option, String value) {
		this.options.put(option, value);
		return this;
	}

	/**
	 * 重置增删改查属性
	 */
	protected void resetOptions() {
		this.options.clear();
		setOption(OPTIONS.TABLE, tableName);
	}

	/**
	 * 检查表是否存在
	 * 
	 * @return
	 */
	public boolean exisit() {
		dbParser.exisitTable(options, tableName);
		if (this.count() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 检查指定表是否存在
	 * 
	 * @return
	 */
	public boolean exisit(String tableName) {
		dbParser.exisitTable(options, tableName);
		if (this.count() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 记录条数
	 * 
	 * @return
	 */
	public int count() {
		return this.count("*");
	}

	/**
	 * 记录条数
	 * 
	 * @return
	 */
	public int count(String field) {
		sql = dbParser.count(options, field);
		this.log();

		ResultSet rs = dbConnection.executeQuery(sql);
		try {
			if (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			logger.error("SQL 错误：" + e.toString());
		} finally {
			this.resetOptions();
		}
		return -1;
	}

	/**
	 * 查询键值对<br/>
	 * <b>注意：键均以大写形式出现</b>
	 * 
	 * @return
	 */
	public List<Map<String, String>> select() {
		sql = dbParser.select(options);
		this.log();

		ResultSet rs = dbConnection.executeQuery(sql);
		try {
			resultList.clear();
			resultList = resultSet2List(rs);
			return new LinkedList<Map<String, String>>(resultList);
		} catch (SQLException e) {
			logger.error("SQL 错误：" + e.toString());
		} finally {
			this.resetOptions();
		}
		return null;
	}

	/**
	 * 将结果集转化为List<br/>
	 * <b>注意：键均以大写形式出现</b>
	 * 
	 * @param rs 结果集
	 * @return
	 * @throws SQLException
	 */
	private List<Map<String, String>> resultSet2List(ResultSet rs)
			throws SQLException {
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int cols = rsmd.getColumnCount();
		while (rs.next()) {
			Map<String, String> resultRow = new LinkedHashMap<String, String>();
			for (int i = 1; i <= cols; i++) {
				resultRow.put(rsmd.getColumnName(i).toUpperCase(),
						rs.getString(i));
			}
			resultList.add(resultRow);
		}
		return resultList;
	}

	/**
	 * 获得第一条
	 * 
	 * @return
	 */
	public Map<String, String> find() {
		this.setOption(OPTIONS.LIMIT, String.valueOf(1));
		this.select();

		if (!resultList.isEmpty()) {
			return resultList.get(0);
		}
		return null;
	}

	/**
	 * 删除记录
	 * 
	 * @return @
	 */
	public int delete() {
		try {
			sql = dbParser.delete(options);
			this.log();

			return dbConnection.executeUpdate(sql);
		} finally {
			this.resetOptions();
		}
	}

	/**
	 * 新增记录
	 * 
	 * @param data
	 * @return
	 */
	public int insert(Map<String, String> data) {
		try {
			sql = dbParser.insert(options, data);
			this.log();

			return dbConnection.executeUpdate(sql);
		} finally {
			this.resetOptions();
		}
	}

	public int insert() {
		try {
			sql = dbParser.insert(options);
			this.log();

			return dbConnection.executeUpdate(sql);
		} finally {
			this.resetOptions();
		}
	}

	/**
	 * 更新记录
	 * 
	 * @param data
	 * @return @
	 */
	public int update(Map<String, String> data) {
		try {
			sql = dbParser.update(options, data);
			this.log();
			
			return dbConnection.executeUpdate(sql);
		} finally {
			this.resetOptions();
		}
	}

	/**
	 * 关闭数据库连接
	 * 
	 * @throws SQLException
	 */
	public void close() throws SQLException {
		dbConnection.close();
	}

	/**
	 * 日志
	 */
	private void log() {
		// 是否需要输出 SQL 语句
		if (SQL_LOGGER) {
			logger.trace(sql);
		}
	}
}
