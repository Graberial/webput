
package database.connection;

import java.io.IOException;
import java.util.Properties;

/**
 * 数据库属性配置
 */
public class Config {
	/**
	 * 偏好配置文件
	 */
	private static Properties properties = new Properties();
	/**
	 * 配置文件的路径
	 */
	private static String CONFIG_PATH = "JDBC.properties";
	/**
	 * 数据库SQL解析类
	 */
	public static String DB_PARSER_CLASS;
	/**
	 * 数据库驱动
	 */
	public static String JDBC_DRIVER;
	/**
	 * JDBC连接URL
	 */
	public static String JDBC_URL;
	/**
	 * 当前使用的数据库
	 */
	public static String JDBC_DATABASE;
	/**
	 * 当前使用的字符集
	 */
	public static String JDBC_CHARSET;
	/**
	 * 数据库用户名
	 */
	public static String JDBC_USER;
	/**
	 * 数据库密码
	 */
	public static String JDBC_PWD;
	/**
	 * 静态初始化偏好设置信息
	 */
	static {
		try {
			// 加载输入流
			properties.load(Config.class.getResourceAsStream(CONFIG_PATH));
			// 获得配置的各个属性
			DB_PARSER_CLASS = properties.getProperty("db.parser.class");
			JDBC_DRIVER = properties.getProperty("jdbc.driver");
			JDBC_URL = properties.getProperty("jdbc.url");
			JDBC_DATABASE = properties.getProperty("jdbc.database");
			JDBC_CHARSET = properties.getProperty("jdbc.charset");
			JDBC_USER = properties.getProperty("jdbc.user");
			JDBC_PWD = properties.getProperty("jdbc.pwd");
		} catch (IOException e) {
			System.err.println("加载数据库配置文件失败："+e.toString());
		}
	}
}
