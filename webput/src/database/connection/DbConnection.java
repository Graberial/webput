package database.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * <b>&lt;单例模式&gt;</b> JDBC 数据库连接类
 * 
 * @author JiangJun
 */
public class DbConnection {
	/**
	 * 实例
	 */
	private static DbConnection dbConnection;

	private Connection connection;
	/**
	 * 执行操作
	 */
	private Statement statement;
	/**
	 * 构造函数
	 */
	public DbConnection() {
		try {
			Class.forName(Config.JDBC_DRIVER);
			connection = DriverManager.getConnection(Config.JDBC_URL+Config.JDBC_DATABASE+"?"+Config.JDBC_CHARSET,
					Config.JDBC_USER, Config.JDBC_PWD);
			System.out.println("连接数据库成功");
		} catch (ClassNotFoundException e) {
			System.err.println("未找到数据库连接类："+e.toString());
		} catch (SQLException e) {
			System.err.println("SQL 错误："+e.toString());
		}
	}
	/**
	 * <b>&lt;线程安全&gt;</b>获取数据库连接实例
	 * 

	 */
	
	public static synchronized DbConnection getDbConnection() {
		if (null == dbConnection) {
			dbConnection = new DbConnection();
		}
		return dbConnection;
	}

	/**
	 * 执行查询操作
	 * 
	 * @param sql 查询 SQL
	 * @return ResultSet 查询结果集
	 */
	public ResultSet executeQuery(String sql) {
		ResultSet result = null;
		try {
			statement = connection.createStatement();
			// 执行SQL语句
			result = statement.executeQuery(sql);
		} catch (SQLException e) {
			System.err.println("SQL 错误："+e.toString());
		}
		return result;
	}

	/**
	 * 执行更新操作
	 * 
	 * @param sql 更新 SQL
	 * @return int 受影响的行数
	 */
	public int executeUpdate(String sql) {
		int rows = -1;
		try {
			statement = connection.createStatement();
			// 执行SQL语句
			rows = statement.executeUpdate(sql);
		} catch (Exception e) {
			System.err.println("SQL 错误："+e.toString());
		}
		return rows;
	}
	
	/**
	 * 执行操作
	 * 
	 * @param sql DDL 等 SQL
	 * @return boolean 执行结果，只要不发生异常均返回 true
	 */
	public boolean execute(String sql) {
		try {
			statement = connection.createStatement();
			// 执行SQL语句
			statement.execute(sql);
			return true;
		} catch (Exception e) {
			System.err.println("错误："+e.toString());
			return false;
		}
	}

	/**
	 * 关闭数据库连接
	 */
	public void close() {
		try {
			if (null != statement) {
				statement.close();
			}
			if (null != connection) {
				connection.close();
			}
		} catch (SQLException e) {
			System.err.println("SQL 错误："+e.toString());
		}
	}
}
