package database.processer;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.dao.DefaultModel;
import database.dao.Model;
import database.objects.Column;
import database.objects.Key;
import database.objects.Table;
import database.sql.DbParser.OPTIONS;
import fetcher.queries.Instance;


/**
 * @author Graberial
 *
 */
public class TableHandler {
	private static Logger logger = LogManager.getLogger();
	
	/*
	 * 需要读写的数据库表信息
	 */
	private String defaultTableName;
	private Table tableInfo;
	public Model model;
	/*
	 * 从数据库中读取的数据表
	 */
	public List<Map<String,String>> relationTable;
	public List<Map<String,String>>sampledTestTable;
	public List<Map<String,String>>sampledTableWithBlanks;
	public List<Map<String,String>> valueConfidences;
	
	public ArrayList<Integer> completeTupleIndexes;
	private ArrayList<Integer> incompleteTupleIndexes;
    public ArrayList<ArrayList<String>> allDicts;
    
      /*  public static void main(String[] args){
		TableHandler myTable = new TableHandler("personinfo");
		myTable.genSampledTable("TestTable");
		Set<String> primaryKey = new HashSet<String>();
		primaryKey.add("NAME");
		primaryKey.add("EMAIL");
		myTable.genSampledTableWithBlanks(primaryKey,0.6f,2,"TableWithBlanks"+60);	
	}*/
    
    
	public TableHandler(){
		tableInfo = new Table();
		relationTable = new ArrayList<Map<String,String>>();
		sampledTestTable = new ArrayList<Map<String,String>>();
		sampledTableWithBlanks = new ArrayList<Map<String,String>>();
		valueConfidences = new ArrayList<Map<String,String>>();
		completeTupleIndexes = new ArrayList<Integer>();
		incompleteTupleIndexes = new ArrayList<Integer>();
		allDicts = new ArrayList<ArrayList<String>>();
		model = new DefaultModel();
	}
	
	public TableHandler(String tableName){
		defaultTableName = tableName;
		tableInfo = new Table(tableName);
		relationTable = new ArrayList<Map<String,String>>();
		sampledTestTable = new ArrayList<Map<String,String>>();
		sampledTableWithBlanks = new ArrayList<Map<String,String>>();
		valueConfidences = new ArrayList<Map<String,String>>();
		completeTupleIndexes = new ArrayList<Integer>();
		incompleteTupleIndexes = new ArrayList<Integer>();
		allDicts = new ArrayList<ArrayList<String>>();
	
		model.setTableName(tableName);
		tableInfo = model.table();
	//	System.out.println(tableName);
	//	System.out.println(model.getSql());
	}

	public void syncTable(List<Map<String,String>> values,Table table ){
		
		Model model = new DefaultModel();
		model.setTable(table);
		if(!model.exisit())
		{
			model.create();
		}
		for(int i =0; i < values.size();i++){
			System.out.println(values.get(i));
			model.insert(values.get(i));
		}
	}


	
	/**读取sampledTestTable和sampledTableWithBlanks
	 * 其实这个也没有多大必要了，因为现在的话sampledTable以及存在数据库中了
	 */
	public void readRelationTable(){
		if(defaultTableName == null){
			System.out.println("defalutTableName is null");
			logger.error("defalutTableName is empty");
			System.exit(0);
		}
		else
		{
			model.setTableName(defaultTableName);
			model.select();
			relationTable.addAll(model.getResultList());
		}
	}
	public int readSampledTableWithBlanks(Set<String> keySet,float percentageOfBlanks){
	
			String tableName =defaultTableName+"test"+(int)(percentageOfBlanks*100);

			tableInfo.setName(tableName);
			model.setTable(tableInfo);
		
			if(model.exisit())
			{
				model.select();
				sampledTableWithBlanks.addAll(model.getResultList());
			}
			else
			{
				System.out.println("tablewithblanks  for name:"+tableName+"doesn't exist\n generating table with blanks……");
				if(!sampledTestTable.isEmpty()){
					genSampledTableWithBlanks(keySet,percentageOfBlanks,2);
				}
				else
				{
					readRelationTable();
					genSampledTable(10);
					genSampledTableWithBlanks(keySet,0.4f,2);
				}
				syncTable(sampledTableWithBlanks,tableInfo);
			}
			int numOfBlanks = 0;
			for(int i=0; i < sampledTableWithBlanks.size();i++){
				Map<String,String>  confidences= new LinkedHashMap<String,String>();
				for(String key: sampledTableWithBlanks.get(i).keySet()){
					if(sampledTableWithBlanks.get(i).get(key).isEmpty()){
						confidences.put(key, "0.00");
						numOfBlanks++;
					}
					else{
						confidences.put(key, "1.00");
					}
				}
				valueConfidences.add(confidences);
			}
			/*Model model = new DefaultModel();
			Table table = new Table();
			table = tableInfo;
			table.setName("confidences");
			model.setTable(table);
			if(!model.exisit())
			{
				model.create();
			}
			else
			{
				model.delete();
			}
			for(int i =0; i < valueConfidences.size();i++){
				System.out.println(valueConfidences.get(i));
				model.insert(valueConfidences.get(i));
			}*/
			return numOfBlanks;
	}
	
	public void readSampledTable(){
		
		tableInfo.setName(defaultTableName+"test");
		model.setTable(tableInfo);
		if(model.exisit()){
			model.select();
			this.sampledTestTable.addAll(model.getResultList());
		}
		else{
			System.out.println("sampleTestTable  for name"+defaultTableName+"Test doesn't exist \n generating sampled table……");
			readRelationTable();
			genSampledTable(20);
			syncTable(sampledTestTable,tableInfo);
		}
	}
   public void genSampledTable(){
		 if(relationTable.isEmpty()){
			 logger.error("relationTablei is empty");
		 }
		 else
		 {
			  for(int i = 0; i < relationTable.size();i++)
			  {
				  sampledTestTable.add(relationTable.get(i));
			  }
		 }
   }
	/**
	 * 从原始relationTable中选取一个用于测试的表，同时将测试表写到DB
	 */
   public void genSampledTable(int tupleNums){
	   if(relationTable.isEmpty()){
			 logger.error("relationTablei is empty");
		 }
		 else
		 {
			  for(int i = 0; i < tupleNums && i <relationTable.size();i++)
			  {
				  sampledTestTable.add(relationTable.get(i));
			  }
		 }
   }

   /**
    * 将测试表sampledTestTable中的某些Cell置为空值，保留至少一个主属性
    * @param keySet Set<String>
    * @param percentageOfBlanks float
    * @param randomSeed int
    * @param newTableName String
    */
   public void genSampledTableWithBlanks( Set<String> keySet,float percentageOfBlanks, int randomSeed){
	   int numOfCells;
	   int numOfBlanks;
	   Random rand = new Random(randomSeed);
	   
	   //首先将sampledTestTable复制到sampledTableWithBlanks
	   for(int i = 0;i < sampledTestTable.size();i++)
	   {
		   Map<String,String> tupleFromRelationTable = new LinkedHashMap<String,String>(sampledTestTable.get(i));
		   sampledTableWithBlanks.add(tupleFromRelationTable);
	   }
	   
	   //根据空缺比例计算表中需要产生多少的空缺值
	   numOfCells = sampledTestTable.size()*sampledTestTable.get(0).size();
	   numOfBlanks = (int)(numOfCells*percentageOfBlanks);
	   
	   //产生空缺值得位置索引
	   ArrayList<Integer>blanksIndexesOfCells = new ArrayList<Integer>();
	 
	   int randomGenBlanksIndex;
	   while(blanksIndexesOfCells.size() < numOfBlanks)
	   {
		randomGenBlanksIndex = rand.nextInt(numOfCells);
		if(!blanksIndexesOfCells.contains(randomGenBlanksIndex))
		{
			blanksIndexesOfCells.add(randomGenBlanksIndex);
		}
	   }

	   //将空缺值索引出的值置为空
	   int tupleIndex, columnIndex;
	   
	   int columnNums = tableInfo.getColumns().size();
	   String[] attributes = new String[sampledTestTable.get(0).size()];
	   attributes = sampledTestTable.get(0).keySet().toArray(attributes);
	  
	   for(int i = 0; i < numOfBlanks;i++)
	   {
		   tupleIndex  = blanksIndexesOfCells.get(i) / columnNums;
		   columnIndex = blanksIndexesOfCells.get(i) % columnNums;
		   this.sampledTableWithBlanks.get(tupleIndex).put(attributes[columnIndex],"");
	   }
	
	  
	   //检测是否有主属性的所有元素都被删除
	   ArrayList<Integer> nonKeyList =  new ArrayList<Integer>();
	   for(int i = 0; i < sampledTableWithBlanks.size();i++)
	   {
		   boolean nonKey = true;
		   for(String key:keySet)
		   {
			 /*  Map<String,String> map = new LinkedHashMap<String,String>();
			   map = sampledTableWithBlanks.get(i);
			   String value = map.get(key);*/
			   if(!sampledTableWithBlanks.get(i).get(key).isEmpty())
			   //!sampledTableWithBlanks.get(i).get(key).equalsIgnoreCase("null")
			{
				   nonKey = false;
			}
			  
		   }
		   if(nonKey == true)
		   {
			   nonKeyList.add(i);
		   }
		   
	   }
	   
	  //如果没有主属性，则增加一个主属性同时删除一个非主属性
	   int randomKeyIndex,randomNonKeyIndex;
	   String [] keys = new String[keySet.size()];
	   keys =keySet.toArray(keys);
	   
	   for(int i = 0; i < nonKeyList.size();i++)
	   {
		   //添加一个主属性
		   int keySetSize = keySet.size();
		//   System.out.println(keySet.size());
		   randomKeyIndex = rand.nextInt(keySetSize);
		   //randomKeyIndex = rand.nextInt(keySet.size());
		   String randomKey = keys[randomKeyIndex];

		   String keyValue = sampledTestTable.get(nonKeyList.get(i)).get(randomKey);
		   sampledTableWithBlanks.get(nonKeyList.get(i)).put(randomKey,keyValue);
	   }  
	   //删除一个非主属性---------------------------------------------这个地方实际上有问题的，因为一个元祖的非主属性可能不存在
	   int numOfNonKeys = tableInfo.getColumnNames().size() - keySet.size();
	   String [] nonKeys  = new String[numOfNonKeys];
	   Set<String>nonKeySet = tableInfo.getColumnNames();
	
	   nonKeySet.removeAll(keySet);
	  // System.out.println(nonKeySet.toString());
	   nonKeys =nonKeySet.toArray(nonKeys);
	   for(int i = 0; i < nonKeyList.size();i++)
	   {
		   randomNonKeyIndex = rand.nextInt(numOfNonKeys);
		   String randomNonKey = nonKeys[randomNonKeyIndex];
		//   System.out.println(randomNonKey);
		   sampledTableWithBlanks.get(nonKeyList.get(i)).put(randomNonKey,"");
	   } 
	  
   }
   public void setTableName(String tableName){
	   defaultTableName = tableName;
	   model.setTableName(tableName);
	   tableInfo = model.table();
   }
	 public void checkTupleCompleteness(){
		 ArrayList<Integer> completeTuples = new ArrayList<Integer>();
		 Map<String,String> tuple = new LinkedHashMap<String,String>();
		 boolean complete;
		 for(int i = 0; i< sampledTableWithBlanks.size();i++)
		 {
			 complete = true;
			 tuple = sampledTableWithBlanks.get(i);
			 for(String key:tuple.keySet())
			 {
				 if(tuple.get(key).isEmpty())
				 {
					 complete = false;
				 }
			 }
			 
			 if(complete == true)
			 {
				 completeTupleIndexes.add(i);
			//	 System.out.println("complete"+i);
				 
			 }
			 else
			 {
				 incompleteTupleIndexes.add(i);
			//	 System.out.println("incomplete"+i);
			 }
		 } 
	 }
	 
	 public ArrayList<Integer> getCompleteTupleIndexes(){
		 ArrayList<Integer> completeTuples = new ArrayList<Integer>();
		 completeTuples.addAll(completeTupleIndexes);
		 return completeTuples;
	 }
	 public ArrayList<Integer> getIncompleteTupleIndexes(){
		 ArrayList<Integer> incompleteTuples = new ArrayList<Integer>();
		 incompleteTuples.addAll(incompleteTupleIndexes);
		 return incompleteTuples;
	 }
	 /**根据给定的属性值，从原始表中生成完整实例的Instance，用于训练每个查询的confidence
	 * @param leveragedAttributes
	 * @param targetAttribute
	 * @return
	 */
	public ArrayList<Instance> getCompleteInstancesByAttributes(ArrayList<String> leveragedAttributes,String targetAttribute){
		 ArrayList<Instance> insts = new ArrayList<Instance>();
		 Map<String,String> tuple = new LinkedHashMap<String,String>();
		 for(int i =0; i< completeTupleIndexes.size();i++)
		 {
			 tuple = sampledTableWithBlanks.get(completeTupleIndexes.get(i));
			 Instance inst = new Instance();
			 for(String key:leveragedAttributes)
			 {
				 inst.leveragedValues.put(key, tuple.get(key));
			 }
			 inst.targetValue.put(targetAttribute, tuple.get(targetAttribute));
			 insts.add(inst);
		 }
		 return insts;
	 }
	
	public ArrayList<Instance> getIncompleteInstanceByTupleIndex(int tupleId){
		ArrayList<Instance> insts = new ArrayList<Instance>();
		 Map<String,String> tuple = new LinkedHashMap<String,String>();
	
		 tuple = sampledTableWithBlanks.get(tupleId);
		 ArrayList<String> existingAttributes = new ArrayList<String>();
		 ArrayList<String> missingAttributes = new ArrayList<String>();
		 for(String key:tuple.keySet()){
			 if(tuple.get(key).isEmpty()){
				 missingAttributes.add(key);
			 }
			 else
			 {
				 existingAttributes.add(key);
			 }
		 }
		 for(int i= 0; i< missingAttributes.size();i++){
			Instance inst = new Instance();
			for(int j = 0; j<existingAttributes.size();j++){
				inst.leveragedValues.put(existingAttributes.get(j), tuple.get(existingAttributes.get(j)));
				//System.out.println( tuple.get(existingAttributes.get(j)));
			}
			inst.targetValue.put(missingAttributes.get(i), "");
		
			insts.add(inst);
		 }
		return insts;
	}
	
	public ArrayList<String> getDictionaryBykey(String key){
		ArrayList<String> dict = new ArrayList<String>();
		/*System.out.println(allDicts);
		int dictIndex = dictionaryIndex(key);
		if(dictIndex > 0){
			dict.addAll(allDicts.get(dictIndex));
			System.out.println("already exist dict");
		}
		else
		{*/
			//System.out.println("dicts for key  "+key+" doesn't exist");
			dict.add(key);
			for(int i =0; i < sampledTestTable.size();i++){
				if(!dict.contains(sampledTestTable.get(i).get(key))){
					dict.add(sampledTestTable.get(i).get(key));
				}
			}
	//		System.out.println(dict.toString());
		//	allDicts.add(dict);

		return dict;
	}
	public int dictionaryIndex(String key){
		if(allDicts.size() == 0){
			return -1;
		}
		for(int i =0; i < allDicts.size();i++){
			if(allDicts.get(i).get(0).equalsIgnoreCase(key)){
				return i;
			}
		}
		return -1;
	}
	public ArrayList<String> getAllAttributes(){
		ArrayList<String> allAttributes = new ArrayList<String>();
		allAttributes.addAll(tableInfo.getColumnNames());
		return allAttributes;
	}
	public Instance getInstance(int tupleId,ArrayList<String> leveragedAttributes,String targetAttribute){
		Instance inst = new Instance();
		for(String key:leveragedAttributes){
			inst.leveragedValues.put(key, sampledTableWithBlanks.get(tupleId).get(key));
		}
		inst.targetValue.put(targetAttribute, sampledTableWithBlanks.get(tupleId).get(targetAttribute));
		return inst;
	}

}

