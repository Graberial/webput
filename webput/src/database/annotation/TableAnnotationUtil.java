package database.annotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import common.utils.StringUtil;

/**
 * 数据表注解工具类
 * 
 * @author JiangJun
 */
public class TableAnnotationUtil {
	/**
	 * 将获得的map结构的数据转换成对应的对象
	 * 
	 * @param po 持久化类
	 * @param map 数据
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public static Object getPO(Object po, Map<String, String> map) throws InstantiationException, IllegalAccessException, SecurityException, IllegalArgumentException, InvocationTargetException{
//		Object po = c.newInstance();
		Class<?> c = po.getClass();
		Field[] fields = c.getDeclaredFields();
		
		for(Field field : fields){
			String methodName = "set"+StringUtil.firstCharToUpperCase(field.getName());
			String value = null;
            if(field.isAnnotationPresent(FieldMapping.class)){
            	FieldMapping fieldMapping = (FieldMapping) field.getAnnotation(FieldMapping.class);
            	value = map.get(fieldMapping.value().toUpperCase());
            } else{
            	value = map.get(field.getName().toUpperCase());
            }
            Method method;
			try {
				method = c.getDeclaredMethod(methodName, new Class[]{String.class});
			} catch (NoSuchMethodException e) {
				continue;
			}
			method.invoke(po, value);
        }
		return po;
	}
	
	/**
	 * 根据对象获得map
	 * 
	 * @param obj
	 * @return
	 * @throws SecurityException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	public static Map<String, String> getData(Object obj) throws SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Map<String, String> data = new HashMap<String, String>();
		Class<?> c = obj.getClass();
		Field[] fields = c.getDeclaredFields();
		for(Field field : fields){
			String methodName = "get"+StringUtil.firstCharToUpperCase(field.getName());
			String fieldName = null;
            if(field.isAnnotationPresent(FieldMapping.class)){
            	FieldMapping fieldMapping = (FieldMapping) field.getAnnotation(FieldMapping.class);
            	fieldName = fieldMapping.value().toUpperCase();
            } else{
            	// 会有一个指向外部类的指针this$0
            	fieldName = field.getName();
            }
            Method method;
			try {
				method = c.getDeclaredMethod(methodName);
			} catch (NoSuchMethodException e) {
				continue;
			}
			
            String value = String.valueOf(method.invoke(obj));
            data.put(fieldName, value);
        }
		return data;
	}
}
