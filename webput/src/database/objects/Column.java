package database.objects;

import java.util.regex.Matcher;

import common.utils.PatternUtil;

/**
 * 最简单的列结构信息
 * 
 * @author jiangjun
 * 
 * @param <T>  默认值类型，即数据列的类型
 */
public class Column<T> implements Cloneable{
	/**
	 * 列名
	 */
	private String name;
	/**
	 * 数据类型
	 */
	private String type;
	/**
	 * 长度
	 */
	private Integer length = null;
	/**
	 * 是否允许空值
	 */
	private Boolean empty = true;
	/**
	 * 默认值
	 */
	private T defaultVal;
	/**
	 * 其他扩展信息
	 */
	private String extra;
	/**
	 * 注释
	 */
	private String comment = "";
	
	public Column(){
		super();
	}
	
	public Column(String name, String type, Integer length) {
		super();
		this.name = name;
		this.type = type;
		this.length = length;
	}

	public Column(String name, String type) {
		super();
		this.name = name;
		Matcher matcher = PatternUtil.matcher(type, "(\\w+)\\((\\d+)\\)");
		if (matcher.find()) {
			this.type = matcher.group(1);
			String length = matcher.group(2);
			if (null != length) {
				this.length = Integer.parseInt(length);
			}
		}
	}
	
	/*-----------------------------------------------------------------------
	 * Getter/Setter
	 * ----------------------------------------------------------------------*/

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Boolean isEmpty() {
		return empty;
	}

	public void setEmpty(Boolean empty) {
		this.empty = empty;
	}

	public T getDefaultVal() {
		return defaultVal;
	}

	public void setDefaultVal(T defaultVal) {
		this.defaultVal = defaultVal;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	/*-----------------------------------------------------------------------
	 * 覆盖equals 和 hashCode
	 * ----------------------------------------------------------------------*/
	
	@Override
	public int hashCode() {
		if (null == name) {
			return super.hashCode();
		}
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Column<?> col = (Column<?>) obj;
		if (null == col || null == col.getName() || null == name) {
			return false;
		}
		if (col.getName().equals(name)) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format("Column : {name = %s , type = %s , length = %d , empty = %s, defaultVal = %s , extra = %s , comment = %s}",
						name, type, length, empty, defaultVal, extra, comment);
	}
	
	/**
	 * 拷贝当前列
	 */
	@Override
	public Object clone(){
		Column<T> newColumn = new Column<T>(name, type, length);
		newColumn.setDefaultVal(defaultVal);
		newColumn.setEmpty(empty);
		newColumn.setExtra(extra);
		return newColumn;
	}
}
