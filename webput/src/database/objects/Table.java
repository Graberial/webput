package database.objects;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import database.dao.DefaultModel;
import database.dao.Model;
import database.objects.Key.Method;
import database.objects.Key.Type;
import database.sql.DbParser.OPTIONS;

/**
 * 数据表对象
 * 
 * @author jiangjun
 */
public class Table {
	/**
	 * 数据表表名
	 */
	private String name;
	/**
	 * 表中的列
	 */
	private Set<Column<?>> columns = new LinkedHashSet<Column<?>>();
	/**
	 * 表中索引
	 */
	private Set<Key> keys = new HashSet<Key>();
	/**
	 * 数据表类型
	 */
	private Engine engine = Engine.INNODB;
	/**
	 * 数据表字符集
	 */
	private Charset charset = Charset.UTF8;
	/**
	 * 注释
	 */
	private String comment = "";

	/**
	 * 数据库中的字符集枚举类型
	 */
	public enum Charset {
		UTF8("utf8"), GBK("GBK"),GB2312("GB2312");
		private String charset;
		
		private Charset(String charset) {
			this.charset = charset;
		}
		
		@Override
		public String toString() {
			return charset;
		}
	}

	/**
	 * 数据表类型枚举类型
	 */
	public enum Engine {
		INNODB("InnoDB"), MYISAM("MyISAM");
		private String engine;

		private Engine(String engine) {
			this.engine = engine;
		}

		@Override
		public String toString() {
			return engine;
		}

		/**
		 * 根据字符串获取数据表类型
		 * 
		 * @param engine 数据表类型字符串
		 * @return
		 */
		public static Engine getEngine(String engine) {
			if (engine != null) {
				try {
					return Enum.valueOf(Engine.class, engine.trim().toUpperCase());
				} catch (IllegalArgumentException e) {
					System.err.println("获取数据库类型错误："+e.toString());
				}
			}
			return null;
		}
	}

	public Table() {
		super();
	}
	
	public Table(String tableName) {
		super();
		this.name = tableName;
	}
	
	/*-----------------------------------------------------------------------
	 * Getter/Setter
	 * ----------------------------------------------------------------------*/

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Set<Column<?>> getColumns() {
		return columns;
	}

	public void setColumns(Set<Column<?>> columns) {
		this.columns = columns;
	}

	public Set<Key> getKeys() {
		return keys;
	}

	public void setKeys(Set<Key> keys) {
		this.keys = keys;
	}
	
	/**
	 * 新增一列
	 * 
	 * @param col Column<?> 列实例
	 */
	public void addColumn(Column<?> col) {
		this.columns.add(col);
	}

	/**
	 * 删除一列
	 * 
	 * @param col Column<?> 列实例
	 */
	public void removeColumn(Column<?> col) {
		this.columns.remove(col);
	}
	
	/**
	 * 获得具体的一列
	 * 
	 * @param columnName
	 * @return
	 */
	public Column<?> getColumn(String columnName){
		if (!columns.isEmpty()) {
			for (Column<?> column : columns){
				if (column.getName().equals(columnName)){
					return column;
				}
			}
		}
		return null;
	}

	/**
	 * 新增索引
	 * 
	 * @param key Key 索引实例
	 */
	public void addKey(Key key) {
		this.keys.add(key);
	}

	/**
	 * 删除索引
	 * 
	 * @param key Key 索引实例
	 */
	public void removeKey(Key key) {
		this.keys.remove(key);
	}

	@Override
	public String toString() {
		return String.format("Table : %s ( Engine = %s , CharSet = %s )\n-Columns :\n%s\n-Keys :\n%s",
						name, engine, charset, columns, keys);
	}
	
	/**
	 * 获得所有列名
	 * 
	 * @return Set 有序集合
	 */
	public Set<String> getColumnNames(){
		Set<String> columnNames = new LinkedHashSet<String>();
		for(Column<?> column : columns){
			columnNames.add(column.getName().toUpperCase());
		}
		return columnNames;
	}

	// 测试方法//
	//public static void main(String[] args) {
//		Table table = new Table();
//		table.setName("personinfo");
//
//		Column<String> col1 = new Column<String>("Name", "varchar", 50);
//		col1.setComment("姓名");
//		Column<String> col2 = new Column<String>("Email", "varchar", 50);
//		col2.setComment("邮箱");
//		Column<String> col3 = new Column<String>("Title","varchar",10);
//		Column<String> col4 = new Column<String>("University","varchar",20);
//		Column<String> col5 = new Column<String>("Country","varchar",20);
//		//col1.setDefaultVal("default");
//		//col2.setEmpty(false);
//		//col2.setComment("测试值");
//
//		table.addColumn(col1);
//		table.addColumn(col2);
//		table.addColumn(col3);
//		table.addColumn(col4);
//		table.addColumn(col5);
//
//		Key pk = new Key();
//		pk.addColumn(col1.getName());
//		pk.addColumn(col2.getName());
//		
////		Key unique = new Key();
////		unique.setName("user");
////		unique.setType(Type.UNIQUE);
////		unique.setMethod(Method.HASH);
////		unique.addColumn(col1.getName());
////		unique.addColumn(col2.getName());
//		
//		table.addKey(pk);
//	//	table.addKey(unique);
//
//		Model model = new DefaultModel();
//		model.setTable(table);
//	    model.setTableName("personinfo");
//		model.setOption(OPTIONS.WHERE, "Title='Mr.'");
//	   model.select();
//	   List<Map<String,String>> data = new ArrayList<Map<String,String>>();
//	   data = model.getResultList();
//	   
//	   //System.out.println(model.attributes());
//	 
//	   Map<String,String> it =new LinkedHashMap<String,String>();
//	   String element;
//	   System.out.println(data.size());
//	   model.setOption(OPTIONS.WHERE,"Country = 'UK'");
//	 
//		   //it = data.get(1);
//		   //it.put("Organization", null);
//	   it.put("Organization", "Soochow");
//		   model.update(it);
//		 //  System.out.println(it);
//	   }
//	   // model.create();
//	 
//	 
//	   
//		//System.out.println(model.create());
//	//System.out.println(model.exisit());
//		//System.out.println(model.select());
////		model.setOption(OPTIONS.WHERE, "id=11");
////		System.out.println(model.delete());
////
////		Map<String, String> data = new HashMap<String, String>();
////		data.put("id", "11");
////		data.put("value", "tests");
////		System.out.println(model.insert(data));
////		System.out.println(model.select());
////
////		model.setOption(OPTIONS.WHERE, "id=11");
////		data.put("value", "testUpdate");
////		System.out.println(model.update(data));
////
////		System.out.println(model.select());
////		System.out.println(model.delete());
////
////		System.out.println(model.columns());
////
//	//System.out.println(model.table());
////		
////		Column<String> newCol2 = new Column<String>("user1", "varchar(200)");
////		System.out.println(model.alterColumn(col2, newCol2));
////		Column<String> col3 = new Column<String>("user", "varchar(200)");
////		System.out.println(model.addColumn(col3));
////		System.out.println(model.columns());
////		
////		System.out.println(model.dropColumn(col3.getName()));
////		System.out.println(model.columns());
////		
////		model.setTableName("test");
////		model.drop();
////
////		model.setTableName("tmall");
////		model.drop();
//		
////		Model model = new DefaultModel();
////		model.setTableName("yesky");
////		model.drop();
////		
////		model.setTableName("detail_yesky");
////		model.drop();
////	model.setTableName("preprocess_yesky");
////		model.table();
//		
//	//	System.out.println(model.attributes());
	}
