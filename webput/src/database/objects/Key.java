package database.objects;

import java.util.HashSet;
import java.util.Set;

import common.utils.StringUtil;

/**
 * 索引结构信息
 * 
 * @author JiangJun
 */
public class Key {
	/**
	 * 索引名称，可为空
	 */
	private String name;
	/**
	 * 索引类型
	 */
	private Type type = Type.PRIMARY;
	/**
	 * 索引方式
	 */
	private Method method = null;
	/**
	 * 索引列名
	 */
	private Set<String> columns = new HashSet<String>();

	/**
	 * 索引类型
	 */
	public enum Type {
		PRIMARY("PRIMARY KEY"), UNIQUE("UNIQUE KEY"), NORMAL("KEY");
		private String key;

		private Type(String key) {
			this.key = key;
		}

		@Override
		public String toString() {
			return key;
		}

		/**
		 * 根据索引名称和索引是否为空获得具体的索引类型
		 * 
		 * @param name 当前索引名称
		 * @param nonUnique 是否非重复
		 * @return
		 */
		public static Type getType(String name, String nonUnique) {
			if (name.equals("PRIMARY")) {
				return PRIMARY;
			} else if (nonUnique.equals("0")) {
				return UNIQUE;
			}
			return NORMAL;
		}
	}

	/**
	 * 索引方式
	 */
	public enum Method {
		BTREE("BTREE"), HASH("HASH");
		private String method;

		private Method(String method) {
			this.method = method;
		}

		@Override
		public String toString() {
			return method;
		}

		/**
		 * 根据索引方法名称获得具体的索引方法
		 * 
		 * @param method 索引方法名称
		 * @return
		 */
		public static Method getMethod(String method) {
			if (method != null) {
				try {
					return Enum.valueOf(Method.class, method.trim()
							.toUpperCase());
				} catch (IllegalArgumentException ex) {
				}
			}
			return null;
		}
	}

	/*-----------------------------------------------------------------------
	 * Getter/Setter
	 * ----------------------------------------------------------------------*/
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public Set<String> getColumns() {
		return columns;
	}

	public void setColumns(Set<String> columns) {
		this.columns = columns;
	}

	/**
	 * 新增索引列
	 * 
	 * @param column 索引列名
	 */
	public void addColumn(String column) {
		this.columns.add(column);
	}

	/**
	 * 删除索引列
	 * 
	 * @param column 索引列名
	 */
	public void removeColumn(String column) {
		this.columns.remove(column);
	}
	
	/*-----------------------------------------------------------------------
	 * 覆盖equals 和 hashCode
	 * ----------------------------------------------------------------------*/

	@Override
	public int hashCode() {
		if (null == name) {
			return super.hashCode();
		}
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Key key = (Key) obj;
		if (null == key || null == key.getColumns() || null == key
				|| null == columns || key.getColumns().size() != columns.size()) {
			return false;
		}
		if (key.getName() != name) {
			return false;
		}
		for (String col : key.getColumns()) {
			if (!columns.contains(col)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return String.format(
				"Key : {name = %s , type = %s , method = %s , columns = %s }",
				name, type, method, StringUtil.join(columns.toArray(), ','));
	}
}
