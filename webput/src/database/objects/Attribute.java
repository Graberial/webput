package database.objects;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import common.utils.MapUtil;
import common.utils.MapUtil.SortType;
import common.utils.StringUtil;

/**
 * 数据库中某一列的所有属性及其出现次数的倒排表
 * 
 * @author JiangJun
 */
public class Attribute {
	/**
	 * 属性名称
	 */
	private String columnName = null;
	/**
	 * 属性值出现次数
	 */
	private Map<String, Integer> valueCounts = new LinkedHashMap<String, Integer>();
	/**
	 * 当前属性值的长度
	 */
	private int length = 0;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public Map<String, Integer> getValueCounts() {
		return valueCounts;
	}

	public void setValueCounts(Map<String, Integer> valueCounts) {
		this.valueCounts = valueCounts;
	}
	
	public void addAll(Map<String, Integer> appendValueCounts) {
		for (String key : appendValueCounts.keySet()) {
			if (valueCounts.get(key) == null) {
				valueCounts.put(key, appendValueCounts.get(key));
			} else {
				valueCounts.put(key, valueCounts.get(key) + appendValueCounts.get(key));
			}
		}
	}

	/**
	 * 获得当前属性值最短长度
	 * 
	 * @return
	 */
	public int getLength() {
		return length;
	}
	
	/**
	 * 获得当前属性值最近的十位数长度
	 * 
	 * @return
	 */
	public int getTenDdigitLength() {
		return (length / 10 + 1) * 10;
	}

	/**
	 * 设置属性长度
	 * 
	 * @param length
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * 新增该列的某一个属性值
	 * 
	 * @param value
	 */
	public void addValue(String value) {
		// 处理掉数字
		value = StringUtil.replaceNumber(value);
		Integer counts = valueCounts.get(value);
		if (null == counts) {
			valueCounts.put(value, 1);
			/**
			 * 当前属性值的长度
			 */
			if (null != value) {
				length = value.length();
			}
		} else {
			valueCounts.put(value, counts + 1);
		}
	}

	/**
	 * 移除某个元素
	 * 
	 * @param value
	 */
	public void removeValue(String value) {
		valueCounts.remove(value);
	}

	/**
	 * 获得该列某个属性值出现次数
	 * 
	 * @param key
	 * @return
	 */
	public Integer getValueCount(String key) {
		Integer valueCount = valueCounts.get(key);
		if (null == valueCount) {
			valueCount = 0;
		}
		return valueCount;
	}

	/**
	 * 获得所有属性值
	 * 
	 * @param key
	 * @return
	 */
	public Set<String> getValues() {
		return valueCounts.keySet();
	}

	/**
	 * 根据出现的次数进行排序
	 * 
	 */
	public void sort() {
		valueCounts = MapUtil.sortByValue(valueCounts, SortType.DESC);
	}

	@Override
	public String toString() {
		return String.format("-Attribute  %s : %s\n", columnName, valueCounts);
	}
}
