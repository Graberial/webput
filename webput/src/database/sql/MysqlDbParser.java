package database.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import common.utils.StringUtil;
import database.connection.Config;
import database.objects.Column;
import database.objects.Key;
import database.objects.Table;
import database.objects.Key.Method;
import database.objects.Key.Type;
import database.objects.Table.Charset;
import database.objects.Table.Engine;

/**
 * Mysql 的数据库解析类<br/>
 * <b>map的key全部默认为大写</b>
 * 
 * 
 * @author JiangJun
 */
public class MysqlDbParser extends DbParser {

	@Override
	public String createTable(Table table) {
		if (null == table) {
			return null;
		}

		String name = table.getName();
		Set<Column<?>> columns = table.getColumns();
		Set<Key> keys = table.getKeys();
		Engine engine = table.getEngine();
		Charset charset = table.getCharset();
		String comment = table.getComment();

		if (null == name || name.isEmpty()) {
			name = "auto_created_table";
		}
		List<String> colsSqlList = new ArrayList<String>();
		if (columns.isEmpty()) {
			return null;
		}
		for (Column<?> col : columns) {
			colsSqlList.add(createColumn(col));
		}
		if (!keys.isEmpty()) {
			for (Key key : keys) {
				colsSqlList.add(createKey(key));
			}
		}

		return String.format("CREATE TABLE `%s` (%s) ENGINE=%s DEFAULT CHARSET=%s COMMENT='%s';",
						name, StringUtil.join(colsSqlList.toArray(), ','),
						engine, charset, comment);
	}

	@Override
	public String createColumn(Column<?> column) {
		if (null == column) {
			return null;
		}

		String name = column.getName();
		String type = column.getType();
		Integer length = column.getLength();
		Boolean empty = column.isEmpty();
		String extra = column.getExtra();

		String comment = column.getComment();
		if (null == name || name.isEmpty()) {
			return null;
		}
		if (null == type || type.isEmpty()) {
			return null;
		}
		if (null != length && length>0) {
			type = type+"("+length+")";
		}
		String notNull = "";
		String defaultValue = "";
		if (null != empty && !empty) {
			notNull = "NOT NULL";
		}
		if (null != column.getDefaultVal()) {
			defaultValue = "DEFAULT '" + String.valueOf(column.getDefaultVal()) + "'";
		}
		if (null == comment) {
			comment = "";
		}
		if (null == extra) {
			extra = "";
		}
		return String.format("`%s` %s %s %s %s COMMENT '%s'", name, type, notNull, extra, defaultValue, comment);
	}

	@Override
	public String createKey(Key key) {
		if (null == key) {
			return null;
		}

		Set<String> columns = key.getColumns();
		String name = key.getName();
		Method method = key.getMethod();
		Type type = key.getType();

		if (columns.isEmpty()) {
			return null;
		}
		// 索引列
		List<String> keys = new ArrayList<String>();
		for (String col : columns) {
			keys.add("`" + col + "`");
			// 取最后一个作为索引名
			if (null == name) {
				name =  col;
			}
		}

		String usingMethod = "";
		if (null != method) {
			usingMethod = "USING " + method.toString();
		}
		return String.format("%s `%s` (%s) %s", type, name,
				StringUtil.join(keys.toArray(), ','), usingMethod);

	}

	@Override
	public String dropTable(String tableName) {
		if (null == tableName) {
			return null;
		}
		return String.format("DROP TABLE IF EXISTS `%s`", tableName);
	}

	@Override
	public void exisitTable(Map<OPTIONS,String> options,String tableName) {
		options.put(OPTIONS.TABLE, "information_schema.TABLES");
		options.put(OPTIONS.WHERE, "table_name='"+tableName+"'");
	}
	
	@Override
	public String showTable(String tableName,String database) {
		if (null == tableName) {
			return null;
		}
		if(null == database){
			database = Config.JDBC_DATABASE;
		}
		return String.format("SHOW TABLE STATUS FROM %s WHERE NAME='%s'",database,tableName);
	}
	
	@Override
	public Table map2Table(Map<String, String> map) {
		if (null == map || map.isEmpty()){
			return null;
		}
		String name = map.get("TABLE_NAME");
		String engine = map.get("ENGINE");
//		String charset = map.get("table_collation");
		Table table = new Table();
		table.setName(name);
		table.setEngine(Engine.getEngine(engine));
		return table;
	}
	
	@Override
	public String showColumns(String tableName){
		if (null == tableName) {
			return null;
		}
		// SHOW FULL COLUMNS FROM %s
		return String.format("SHOW COLUMNS FROM %s", tableName);
	}
	
	@Override
	public Column<?> map2Column(Map<String,String> map){
		if (null == map || map.isEmpty()){
			return null;
		}
		String name = map.get("COLUMN_NAME");
		String type = map.get("COLUMN_TYPE");
		String def = map.get("COLUMN_DEFAULT");
		Boolean empty = map.get("IS_NULLABLE").equals("NO") ? false : true;
		String extra = map.get("EXTRA");
		Column<String> column = new Column<String>(name, type);
		column.setEmpty(empty);
		column.setDefaultVal(def);
		column.setExtra(extra);
		return column;
	}
	
	@Override
	public String showKeys(String tableName) {
		if (null == tableName) {
			return null;
		}
		return String.format("SHOW KEYS FROM %s", tableName);
	}

	@Override
	public Key map2Key(Map<String, String> map) {
		if (null == map || map.isEmpty()){
			return null;
		}
		String column = map.get("COLUMN_NAME");
		String name = map.get("INDEX_NAME");
		String nonUnique = map.get("NON_UNIQUE");
		String method = map.get("INDEX_TYPE");
		Key key = new Key();
		key.setName(name);
		key.setType(Type.getType(name, nonUnique));
		key.setMethod(Method.getMethod(method));
		key.addColumn(column);
		return key;
	}
	
	@Override
	public String alterColumn(String tableName, Column<?> oldColumn, Column<?> newColumn){
		if (null == tableName || null == oldColumn || null == newColumn) {
			return null;
		}
//		return String.format("ALTER TABLE %s CHANGE COLUMN `%s` `%s` %s(%d);", tableName, oldColumn.getName(), newColumn.getName(), newColumn.getType(), newColumn.getLength());
		return String.format("ALTER TABLE %s CHANGE COLUMN `%s` %s", tableName, oldColumn.getName(), createColumn(newColumn));
	}

	@Override
	public String dropColumn(String tableName,String columnName){
		if (null == tableName || null == columnName) {
			return null;
		}
		return String.format("ALTER TABLE %s DROP COLUMN `%s`", tableName, columnName);
	}
	
	@Override
	public String addColumn(String tableName,Column<?> column){
		if (null == tableName || null == column) {
			return null;
		}
		return String.format("ALTER TABLE %s ADD COLUMN %s", tableName, createColumn(column));
	}
	
	@Override
	public String copyTableStruct(String tableName, String newTableName){
		if (null == tableName || null == newTableName) {
			return null;
		}
		return String.format("CREATE TABLE `%s` LIKE `%s`", newTableName, tableName);
	}
	
	@Override
	public String copyTable(String tableName, String newTableName){
		if (null == tableName || null == newTableName) {
			return null;
		}
		return String.format("INSERT INTO `%s` (SELECT * FROM `%s`);", newTableName, tableName);
	}
	
	/*----------------------------------------------------
	 * 数据查询相关
	 *--------------------------------------------------*/
	@Override
	public String parserSql(SQLTemplete sqlTemplete,Map<OPTIONS, String> options) {
		sql = sqlTemplete.toString();

		//解析所有参数
		for(OPTIONS option : OPTIONS.values()){
			String value = options.get(option);
			if(null == value){
				switch(option){
				case FIELD:
					value = "*";
					break;
				default:
					value = "";	
				}
			} else{
				switch(option){
				case DISTINCT:
					value = "DISTINCT " + value + " ";
					break;
				case WHERE:
					value = " WHERE " + value;
					break;
				case GROUP:
					value = " GROUP BY " + value;
					break;
				case HAVING:
					value = " HAVING " + value;
					break;
				case ORDER:
					value = " ORDER BY " + value;
					break;
				case LIMIT:
					value = " LIMIT " + value;
					break;
				default:
				}
			}
			sql =  sql.replace("%"+option.toString()+"%", value);
		}
		sql = sql.replaceAll("\\s+", " ");
		return sql;
	}

	@Override
	public String select(Map<OPTIONS,String> options) {
		return parserSql(SQLTemplete.SELECT_SQL, options);
	}

	@Override
	public String count(Map<OPTIONS,String> options,String field) {
		if (null == field || field.isEmpty()) {
			field = "*";
		}
		field = "COUNT("+field+")";
		options.put(OPTIONS.FIELD, field);
		options.put(OPTIONS.LIMIT, String.valueOf(1));
		return select(options);
	}

	@Override
	public String delete(Map<OPTIONS, String> options) {
		return parserSql(SQLTemplete.DELETE_SQL, options);
	}

	@Override
	public String insert(Map<OPTIONS, String> options, Map<String, String> data) {
		if(null == data || data.isEmpty()){
			return null;
		}
		List<String> fields = new ArrayList<String>();
		List<String> values = new ArrayList<String>();
		for(String field : data.keySet()){
			fields.add("`"+field+"`");
			values.add("'"+data.get(field)+"'");
		}
		
		options.put(OPTIONS.FIELD, StringUtil.join(fields.toArray(), ','));
		options.put(OPTIONS.DATA, StringUtil.join(values.toArray(), ','));
		return this.insert(options);
	}

	@Override
	public String insert(Map<OPTIONS, String> options) {
		return parserSql(SQLTemplete.INSERT_SQL, options);
	}
	
	@Override
	public String update(Map<OPTIONS, String> options, Map<String, String> data) {
		if(null == data || data.isEmpty()){
			return null;
		}
		List<String> values = new ArrayList<String>();
		for(String field : data.keySet()){
			values.add(String.format("`%s` = '%s'", field,data.get(field)));
		}
		
		options.put(OPTIONS.DATA, StringUtil.join(values.toArray(), ','));
		return this.update(options);
	}

	@Override
	public String update(Map<OPTIONS, String> options) {
		return parserSql(SQLTemplete.UPDATE_SQL, options);
	}
}
