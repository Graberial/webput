package database.sql;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.connection.Config;
import database.objects.Column;
import database.objects.Key;
import database.objects.Table;

/**
 * 解析正确的SQL
 * 
 * @author jiangjun
 */
public abstract class DbParser {
	/**
	 * 日志
	 */
	protected static Logger logger = LogManager.getLogger();
	/**
	 * 当前增删改查的SQL
	 */
	protected String sql;
	
	/**
	 * 查询语句模板
	 */
	public enum SQLTemplete{
		SELECT_SQL("SELECT %DISTINCT% %FIELD% FROM %TABLE% %JOIN% %WHERE% %GROUP% %HAVING% %ORDER% %LIMIT% %UNION%"),
		DELETE_SQL("DELETE FROM %TABLE% %WHERE%"),
		UPDATE_SQL("UPDATE %TABLE% SET %DATA% %WHERE%"),
		INSERT_SQL("INSERT INTO %TABLE%(%FIELD%) VALUES(%DATA%)");
		private String templete;
		private SQLTemplete(String templete){
			this.templete = templete;
		}
		@Override
		public String toString(){
			return templete;
		}
	}
	
	/**
	 * 增删该查需要用到的一些参数
	 */
	public enum OPTIONS{
		DISTINCT,FIELD,TABLE,JOIN,WHERE,GROUP,HAVING,ORDER,LIMIT,UNION,DATA
	}
	
	/**
	 * 获取具体的数据库实例
	 * 
	 * @return
	 */
	public static DbParser getInstance() {
		try {
			return (DbParser) Class.forName(Config.DB_PARSER_CLASS).newInstance();
		} catch (Exception e) {
			logger.error("获取数据库连接实例错误："+e.toString());
		}
		return null;
	}

	/*----------------------------------------------------
	 * 数据表操作相关
	 *--------------------------------------------------*/
	/**
	 * 创建表SQL
	 * 
	 * @param table
	 * @return
	 */
	public abstract String createTable(Table table);

	/**
	 * 创建列SQL
	 * 
	 * @param column
	 * @return
	 */
	public abstract String createColumn(Column<?> column);

	/**
	 * 创建索引SQL
	 * 
	 * @param key
	 * @return
	 */
	public abstract String createKey(Key key);

	/**
	 * 删除表SQL
	 * 
	 * @param tableName
	 * @return
	 */
	public abstract String dropTable(String tableName);

	/**
	 * 检查表是否存在，设置options参数（TABLE/WHERE）
	 * 
	 * @param tableName
	 * @return
	 */
	public abstract void exisitTable(Map<OPTIONS,String> options,String tableName);
	
	// 获得表结构
	/**
	 * 显示某张表信息SQL
	 * 
	 * @param tableName
	 * @param database
	 * @return
	 */
	public abstract String showTable(String tableName,String database);
	
	/**
	 * 将showTable()执行后获取到的表信息map转化为Table对象
	 * 
	 * @param map
	 * @return
	 */
	public abstract Table map2Table(Map<String,String> map);
	
	/**
	 * 获得所有数据列SQL
	 * 
	 * @param tableName
	 * @return
	 */
	public abstract String showColumns(String tableName);
	
	/**
	 * 将showColumns()执行后获取到的列信息map转化为Column对象
	 * 
	 * @param map
	 * @return
	 */
	public abstract Column<?> map2Column(Map<String,String> map);
	/**
	 * 获得所有索引结构信息SQL
	 * 
	 * @param tableName
	 * @return
	 */
	public abstract String showKeys(String tableName);
	/**
	 * 将showKeys()执行后获取到的索引信息map转化为Key对象
	 * 
	 * @param map
	 * @return
	 */
	public abstract Key map2Key(Map<String,String> map);
	
	/**
	 * 修改列SQL
	 * 
	 * @param tableName
	 * @param oldColumn 原来的列信息
	 * @param newColumn 新的列信息
	 * @return
	 */
	public abstract String alterColumn(String tableName, Column<?> oldColumn, Column<?> newColumn);

	/**
	 * 删除列SQL
	 * 
	 * @param tableName
	 * @param columnName 删除列的名称
	 * @return
	 */
	public abstract String dropColumn(String tableName,String columnName);
	
	/**
	 * 新增列SQL
	 * 
	 * @param tableName
	 * @param column 新增列信息
	 * @return
	 */
	public abstract String addColumn(String tableName,Column<?> column);
	
	/**
	 * 复制表及数据SQL
	 * 
	 * @param tableName
	 * @param newTableName 新表名称
	 * @return
	 */
	public abstract String copyTableStruct(String tableName, String newTableName);
	/**
	 * 复制表及数据SQL
	 * 
	 * @param tableName
	 * @param newTableName 新表名称
	 * @return
	 */
	public abstract String copyTable(String tableName, String newTableName);
	
	/*----------------------------------------------------
	 * 数据查询相关
	 *--------------------------------------------------*/
	
	/**
	 * 解析SQL模版
	 * 
	 * @param sqlTemplete
	 * @param options
	 * @return
	 */
	protected abstract String parserSql(SQLTemplete sqlTemplete,Map<OPTIONS,String> options);
	
	/**
	 * 获得查询SQL
	 * 
	 * @param options
	 * @return
	 */
	public abstract String select(Map<OPTIONS,String> options);

	/**
	 * 统计行数SQL
	 * 
	 * @param options
	 * @param field
	 * @return
	 */
	public abstract String count(Map<OPTIONS,String> options,String field);

	/*----------------------------------------------------
	 * 数据新增、删除、更新相关
	 *--------------------------------------------------*/
	/**
	 * 删除SQLS
	 * 
	 * @param options
	 * @return
	 */
	public abstract String delete(Map<OPTIONS,String> options);
	
	/**
	 * 新增SQL
	 *  
	 * @param options
	 * @return
	 */
	public abstract String insert(Map<OPTIONS,String> options);
	public abstract String insert(Map<OPTIONS,String> options,Map<String,String> data);
	
	/**
	 * 更新SQL
	 * 
	 * @param options
	 * @return
	 */
	public abstract String update(Map<OPTIONS,String> options);
	public abstract String update(Map<OPTIONS,String> options,Map<String,String> data);
	
}