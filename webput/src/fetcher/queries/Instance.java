package fetcher.queries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.LinkedHashMap;
public class Instance {
	public int tupleId;
	public Map<String,String> leveragedValues;
	public Map<String,String> targetValue;
	public float confOfInstance; //for init instance, the conf is 1.f, for learned instance, the conf is the sum of all leveraged Values and target Value.
	
	public Instance() {
		this.tupleId = -1;
		this.leveragedValues = new LinkedHashMap<String,String>();
		this.targetValue = new LinkedHashMap<String,String>();
		this.confOfInstance = 1.f;
	}
	
	public Instance(Map<String,String> values, int tupleId) {
		this.leveragedValues = new LinkedHashMap<String,String>();
		this.leveragedValues.putAll(values);
		this.tupleId = tupleId;
		this.targetValue = new LinkedHashMap<String,String>();
		this.confOfInstance = 1.f;
	}
	
	public Instance( int tupleId,Map<String,String> values, String targetValue) {
		this.leveragedValues = new LinkedHashMap<String,String>();
		this.leveragedValues.putAll(values);
		this.targetValue = new LinkedHashMap<String,String>();
		this.tupleId = tupleId;
		this.confOfInstance = 1.f;
	}
	public ArrayList<String> getLeveragedAttributes(){
		ArrayList<String> utilizedAttributes = new ArrayList<String>();
		
		for(String key:leveragedValues.keySet())
		{
			
			utilizedAttributes.add(key);
		}
		return utilizedAttributes;
	}
	
	public String getTargetAttribute(){
		String targetAttribute = targetValue.keySet().toString();
		targetAttribute = targetAttribute.replace("[", " ");
		targetAttribute = targetAttribute.replace("]", " ");
		targetAttribute = targetAttribute.trim();
		return targetAttribute;
		
	}
	 /**将leveraged Values转化为ArrayList的形式
	 * @return
	 */
	public ArrayList<String> getLeveragedValues(){
		 ArrayList<String> arraylistLeveragedValues = new ArrayList<String>();
		 for(String key:leveragedValues.keySet()){

			 arraylistLeveragedValues.add(leveragedValues.get(key));
		 } 
		 return arraylistLeveragedValues;
	 }
	public String getTargetValue(){
		ArrayList<String> value = new ArrayList<String>();
		for(String key:targetValue.keySet()){
			value.add(targetValue.get(key));
		}
		return value.get(0);
	}
	public boolean isEqual(Instance anotherOne) {
		
		if (anotherOne.leveragedValues.equals(leveragedValues) && anotherOne.targetValue.equals(targetValue)
			&& anotherOne.tupleId == tupleId)
		{
			return true;
		}
		return false;
	}
	
	
}
