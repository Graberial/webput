package fetcher.implementation;

public class EnDeURL {

		public static String enUrl (String inputUrl) {
			String url = new String(inputUrl);
			url = url.replaceAll("\\%", "%25");
			url = url.replaceAll("\\?", "%3f");
			url = url.replaceAll("\\=", "%3d");
			url = url.replaceAll("\\&", "%26");
			url = url.replaceAll("\\;", "%3b");
			url = url.replaceAll("\\/", "%2f");
			url = url.replaceAll("\\:", "%3a");
			url = url.replaceAll("\\#", "%23");
			url = url.replaceAll("\\+", "%2b");
			url = url.replaceAll("\\$", "%24");
			url = url.replaceAll("\\,", "%2c");
			url = url.replaceAll("\\s+", "%20");
			url= url.replaceAll("\\@", "%40");
			return url;
		}
		
		public static String deUrl(String inputUrl) {
			String url = new String(inputUrl);
			url = url.replaceAll("\\%3f", "?");
			url = url.replaceAll("\\%3d", "=");
			url = url.replaceAll("\\%26", "&");
			url = url.replaceAll("\\%3b", ";");
			url = url.replaceAll("\\%2f", "/");
			url = url.replaceAll("\\%3a", ":");
			url = url.replaceAll("\\%23", "#");
			url = url.replaceAll("\\%2b", "+");
			url = url.replaceAll("\\%24", "$");
			url = url.replaceAll("\\%2c", ",");
			url = url.replaceAll("\\%20", " ");
			url = url.replaceAll("\\%25", "%");
			url = url.replaceAll("\\%3c", "<");
			url = url.replaceAll("\\%3e", ">");
			url = url.replaceAll("\\%7e", "~");
			return url;
		}
}
