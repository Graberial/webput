package fetcher.implementation;
import extractor.base.Extractor;
import extractor.base.Snippets;
import extractor.base.SnippetsStore;
import fetcher.queries.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import setExpansion.functions.*;

import com.leeonJava.fileDispose.ReadWriteTxt;
import com.leeonJava.internet.EnDeUrl;
import com.leeonJava.internet.HtmlofUrl;
import com.leeonJava.textDispose.MatchRegexinFile;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 * @author Graberial
 *
 */
//https://api.datamarket.azure.com/Bing/SearchWeb/v1/Web?Query=%27andywilson%40gmail.com%27&Market=%27en-US%27&Options=%27EnableHighlighting%27
public class Fetcher {
	//硬编码，需要继续修改成配置文件
	 public static String tempPath ="C:/Users/Graberial/workspace/webput/WebContent/WEB-INF/classes/";
	
	public static void main(String args[]){
		ArrayList<String> searchTerms =new ArrayList<String>();
		searchTerms.add("andywilson@gmail.com");
		
		String htmlpage = new String();
		htmlpage = Fetcher.graspPosQueryWeb(searchTerms);
		//System.out.println(htmlpage);
		if(htmlpage.isEmpty()){
			System.out.println("error in HtmlFetcher.getBingApiResult,");
		}
		else{
			System.out.println(htmlpage);
		}
	}
	
	
	
	/**直接拼凑查询语句，使用Bing 搜索获得与搜索对象相关的URL链接
	 * @param seachTerms  要查询的关键字
	 * @param resultNum   想要获取的URL数量
	 * @return   urls      由搜索引擎返回的相关链接数量
	 */
	public static ArrayList<String> getBingURLs(String seachTerms, int resultNum) {
		String htmlpage = getBingSearchResultPage(seachTerms,resultNum);
		ArrayList<String> urls = new ArrayList<String>();
		ArrayList<String> allUrls = MatchRegexinFile.getAllMatch("\\<a href=\"http([^\"]+)\"", htmlpage);
		for (int j = 0; j < allUrls.size(); j ++) {
			if (!allUrls.get(j).toLowerCase().contains("bing.com") && !allUrls.get(j).toLowerCase().contains("microsoft") && !allUrls.get(j).toLowerCase().contains("cache")) {
				String temp = new String(allUrls.get(j).split("\"")[1]);
				urls.add(temp);
				System.out.println(temp);
			}
		}
	//	System.out.println(urls.size());
		return urls;
	}
	
	/** 由索要查询的Terms，获得bing搜索返回页面
	 * @param searchTerms 
	 * @param resultNum
	 * @return 原始的bing搜索返回页面
	 */
	public static String getBingSearchResultPage(String searchTerms,int resultNum)
	{
		ArrayList<String> urls = new ArrayList<String>();
		searchTerms = searchTerms.replaceAll("([\\s]+)", "+"); // use + instead of space
	//http://global.bing.com/?FORM=HPCNEN&setmkt=en-us&setlang=en-ushttp://global.bing.com/?FORM=HPCNEN&setmkt=en-us&setlang=en-us
		String urlstr = new String("http://www.gfsoso.com/scholar?q=" + searchTerms);//+"&hl=en&num="+resultNum
		System.out.println(urlstr);
		String htmlpage = getHtmlforUrl(urlstr);
		
		return htmlpage;
	}
	
	/**由给定的inst生成文件存储路径，不管此html页面是否存在，文件名包括文件后缀！这里统一设置为.html
	 * @param inst
	 * @return
	 */
	public static String constructPostFileName(ArrayList<String> leveragedValues)
	{
		int i=0;
		String fileName ="";
		
		Collections.sort(leveragedValues);
		for(i =0;i < leveragedValues.size()-1;i++)
		{
			fileName = fileName.concat(leveragedValues.get(i)+"+");
		}
		fileName = fileName.concat(leveragedValues.get(i));
		fileName = fileName.concat(".html");
		return fileName;	
	}
	
	/**由给定的inst生成查询的完整query。www.global.bing.com/search?q=……
	 * @param inst
	 * @return
	 */
	public static String constructPostURL(Instance inst,int startNum)
	{
	
		String query ="http://www.cn.bing.com/search?q=";
	//	String query ="http://www.gfsoso.com/?q=";
		for(String key:inst.leveragedValues.keySet())
		{
			try{
				query = query.concat(URLEncoder.encode(inst.leveragedValues.get(key),"utf-8")+"+");
			}
			catch(Exception e)
			{
				System.out.println("Encoding URL error in HtmlFetcher.constructPostURL");
				e.printStackTrace();
			}
		}
		query = query.substring(0,query.length()-2);
		//query = query.trim();
		query = query.concat("&go=&start=0&first=200&qs=bs&intlF=1&FORM=TIPEN1");
	//	query = query.concat("&hl=en&lr=lang_en&start="+startNum);
		return query;
	}
	
	public static String encodeSearchTerms(ArrayList<String> leveragedValues){
		int i=0;
		String searchTerms = "";
		try{
			for(i =0; i < leveragedValues.size()-1;i++){
			
				searchTerms = searchTerms.concat(EnDeURL.enUrl(leveragedValues.get(i))+"%20");
			}
			searchTerms = searchTerms.concat(EnDeURL.enUrl(leveragedValues.get(i)));
		
		}catch(Exception e){
			e.printStackTrace();
		}
	//	System.out.println(searchTerms);
		return searchTerms;
	}
/*	public static void graspPosQueryWeb(ArrayList<String> leveragedValues){
		int i=0;
		String fileName = "";
		String htmlpage ="";
		for(i=0; i < leveragedValues.size()-1;i++){
			fileName = fileName.concat(leveragedValues.get(i)+"+");
		}
		fileName = fileName.concat(leveragedValues.get(i));
		String queryFilePath = "res/BingSearchApi/"+fileName+".html";
		File file = new File(queryFilePath);
		if(!file.exists())
		{
			htmlpage = HtmlFetcher.getBingApiResults(inst)
		}
		
	}*/
	/**由一组Instance获取Bing Search 的结果页面
	 * @param insts 具体的实例
	 */
	
	public static String graspPosQueryWeb(ArrayList<String> leveragedValues) {
		//抓取Instance生成查询的搜索页面
	
		String htmlpage ="";
		String fileName = constructPostFileName(leveragedValues);//首先尝试从文件读取Html页面。
		
		if (fileName.isEmpty()) {               //如果给定的用于查询的关键词都是空，则次查询没有必要进行下去
		 System.out.println("leveraged value to search bing is empty");
		}
		else
		{
			String queryFilePath =tempPath+"res/BingSearchApi/"+fileName;
			
			File file = new File(queryFilePath);
			if (file.exists()) {//如果文件存在
				System.out.println("Fetching htmlpage for:"+fileName);
				htmlpage = FetchHtmlTemp.readHtmlfromFileinTxt(queryFilePath);
			}
			else
			{
				htmlpage = Fetcher.getBingApiResults(leveragedValues);
				if(htmlpage == null){
					System.out.println("error in HtmlFetcher.getBingApiResult,");
				}
				else
				{
					FetchHtmlTemp.writeHtmlintoFileinTxt(htmlpage,queryFilePath);
				}	
			}
		}
		return htmlpage;
	}

	/**使用Bing Api的查询
	 * @param searchTerms
	 * @return
	 */
	/*public static ArrayList<String> fetchSnippetsForOneInstance(Instance inst){
		ArrayList<String> mySnippets = new ArrayList<String>();
		String fileName = Fetcher.constructPostFileName(inst.getLeveragedValues());
		String htmlpage ="";
		String queryFilePath ="res/BingSearchApi/"+fileName;
		File file = new File(queryFilePath);
		if(file.exists())
		{
			htmlpage = ReadWriteTxt.readFile(queryFilePath, "");
		}
		else
		{
			String searchTerms = Fetcher.encodeSearchTerms(inst.getLeveragedValues());
			
			htmlpage = Fetcher.getBingApiResults(inst.getLeveragedValues());
			FetchHtmlTemp.writeHtmlintoFileinTxt(htmlpage, queryFilePath);
				
		}
		mySnippets = Extractor.extractSnippetsFromJSON(htmlpage);
		return mySnippets;
	}*/
	/**对某个Instance进行查询，返回此查询的一串相关片段
	 * @param leveragedValues
	 * @return  Snippets
	 *//*
	public static ArrayList<String> fetchSnippetsForOneInstance(Instance inst){
		ArrayList<String> mySnippets = new ArrayList<String>();
		String queryFilePath = constructPostFileName(inst);
		String htmlpage ="";
		//首先看由此Instance查询的网页是否已经捕获了
		File queryFile = new File(queryFilePath);
		if(queryFile.exists())
		{
			htmlpage = ReadWriteTxt.readFile(queryFilePath, "");
		}
		else
		{
			String urlstr = constructPostURL(inst,20);

			try {
					htmlpage = getHtmlforUrl(urlstr);
					if (htmlpage == null) 
					{
						System.err.println("getting webpage error");
						System.exit(0);
					}
					FetchHtmlTemp.writeHtmlintoFileinTxt(htmlpage, queryFilePath);
				} catch (Exception e)
				{
					System.err.println(urlstr);
				}
		}
		mySnippets = Extractor.extractSnippets(htmlpage);
		return mySnippets;
	}*/
	/**由给定的url，读取次url的网页文档
	 * @param urlstr 
	 * @return 
	 */
	public static String getHtmlforUrl(String urlstr) {
		String currentLine = new String();
		String totalString = new String();
		
		try {
	
			URL url = new URL(urlstr);
			HttpURLConnection http = (HttpURLConnection) url.openConnection();
			http.setRequestProperty("User-Agent", "Mozilla/4.0");
			http.connect();
			String encoding = http.getContentEncoding();
			
			if (encoding == null) {
				InputStream urlstream = http.getInputStream();
				BufferedReader l_reader = new BufferedReader(new java.io.InputStreamReader(urlstream));
				
				while ((currentLine = l_reader.readLine()) != null) {
					System.out.println(currentLine);
					if (MatchRegexinFile.getFirstMatch("content=\"text/html;\\s*charset=", currentLine) != null) {
						encoding = MatchRegexinFile.getFirstMatch("charset=([^\"]+)", currentLine);
						encoding = encoding.split("charset=")[1];
						break;
					}
				}
			}
			
			
			http = (HttpURLConnection) url.openConnection();
			http.setRequestProperty("User-Agent", "Mozilla/4.0");
			http.setConnectTimeout(10000);
			http.connect();
			InputStream urlstream = http.getInputStream();
			BufferedReader l_reader = new BufferedReader(new java.io.InputStreamReader(urlstream, encoding));
			
			while ((currentLine = l_reader.readLine()) != null) {
				totalString = totalString.concat(currentLine + "\n");
			}
			
			Random rand = new Random();
			Thread.sleep(3000+rand.nextInt(5000));
		}  catch (IOException e) {
			System.err.println("Some wrong in getHtmlforUrl!");
			totalString = null;
			e.printStackTrace();
		} catch (Exception e) {
		
			e.printStackTrace();
		};
		
		return totalString;
	}
		
	/**给定要查询的关键词，使用Bing Api查询结果
	 * @param searchTerms
	 * @return 查询的JSONObject被转化为String 返回。
	 */
	public static String getBingApiResults(ArrayList<String> leveragedValues) {
			String htmlpage="";
			String searchQuery = "Query=%27";	
			String searchUrl = "https://api.datamarket.azure.com/Bing/SearchWeb/v1/Web?";	
			String searchTerms = Fetcher.encodeSearchTerms(leveragedValues);
			String searchParam = "%27&$top=20&$format=JSON"; //&Market=%27en-US%27&$format=JSON&$top=50
			URL url = null;
		
			URLConnection connection = null;
			String line = null;
			try {
				System.out.println(searchUrl+searchQuery+searchTerms+searchParam);
				url = new URL(searchUrl+searchQuery+searchTerms+searchParam);
				String username = "757e89ee-676e-4d2e-8920-cd6468e9caaa";
			       String password= "pvk0yGVoB9kp2mLOxxicBbQtfh+zAbcpRD+TuPKsx/8";
				connection = url.openConnection();
				String userpass = username + ":" + password;
				String basicAuth = "Basic "
						+ new String(new Base64().encode(userpass.getBytes()));
				connection.setRequestProperty("Authorization", basicAuth);

				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						connection.getInputStream()));
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				htmlpage = builder.toString();

				builder = null;
				reader = null;
				connection = null;
				url = null;
			} catch (Exception e) {
				System.out.println("get webpage from internet error in HtmlFetcher.getBingApiResult()");
				e.printStackTrace();
			}
		return htmlpage;
	}
}
	
