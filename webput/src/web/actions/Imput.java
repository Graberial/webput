package web.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import webput.core.WebPut;

public class Imput extends HttpServlet {
@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			// TODO Auto-generated method stub
		resp.setContentType("text/html:charset=UTF-8");
		PrintWriter pw = resp.getWriter();
		int tupleId = Integer.parseInt(req.getParameter("tupleId"));
		WebPut webput = WebPut.getWebPut();
	
		Map<String,String> tuple = new LinkedHashMap<String,String>();
	
		tuple = webput.processOneTuple(tupleId);
		System.out.println(tuple);
		for(String key:tuple.keySet()){
			pw.print("<td>"+tuple.get(key)+"</ td>");
		}
		pw.flush();
		pw.close();
		}
}
