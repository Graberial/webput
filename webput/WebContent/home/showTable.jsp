
<%@page import="webput.core.WebPut"%>
<%@page import="database.objects.Table"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="database.dao.*" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>webput</title>
<script type="text/javascript">

	var xmlhttp;
	var id =0;
	function createXMLHttpRequest()
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
	}
	function showResults(id){
	
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
			
		  document.getElementById(id).innerHTML=xmlhttp.responseText;
		    
		    }
		}
	
	function imput(){
		if(id <59)
			{
			 createXMLHttpRequest();
//				for(var i=1; i < 10; i++){
					xmlhttp.onreadystatechange=function(){showResults(id-1)};
					var url ="http://localhost:8080/webput/process?tupleId="+id;
					xmlhttp.open("post",url,true);
					xmlhttp.send(); 
			}

			
	//	}
		id++;

	}
	function continuous()
	{
		setInterval(imput,3000);		
	}
	
</script>
</head>
<h1>Webput</h1>
<!--  <%@include file="/common/header.jsp" %> -->
<!-- <a href="<%= request.getContextPath() %>/home/showTable.jsp" >InitTable</a> -->
<body>
<button type="button"  onclick="">Reset</button>
<button type="button"  onclick="imput()">Step by Step</button>
<button type="button"  onclick="continuous()"> Continuous</button>

<%
	List<Map<String,String>> rs = new ArrayList<Map<String,String>>();
	Model model = new DefaultModel();
	model.setTableName("personinfotest40");
	model.select();
	rs = model.getResultList(); 

%>
<table border="1">
<tr>
<td>Name</td> <td>Email</td> <td>Title</td> <td>University</td> <td>Country</td>

<%
	Map<String,String> tuple = new HashMap<String,String>();

 	for(int i =0; i < rs.size();i++){
 		tuple = rs.get(i);
 	
 	%>
 	<tr id="<%=i %>">
 		<%
 		for(String key:tuple.keySet()){
 		%>
 		<td><%=tuple.get(key) %></td>
 		<%		
 		}
 		%>
 		
 	</tr>
 	<% 
 	}
%>
</tr>

</table>
</body>
</html>